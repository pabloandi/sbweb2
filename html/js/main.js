function cargarSeleccionFichas()
{
 
 jQuery(".fila_ficha").each(
	function(i,e){ 
		jQuery(this).click(function(){jQuery(this).toggleClass("seleccionado")});
	});
	
}

function seleccionarFichas(){
		var seleccionados=[];
		jQuery('.seleccionado').each(function(){ seleccionados.push({name:'fichas[]',value:this.id}) });
		return jQuery.param(seleccionados);
}

function resaltarPalabra(word)
{
    jQuery("tr:contains('"+word+"')").css("text-decoration", "underline");
    
}

function cargarTabla()
{
//     jQuery("#tabla_consulta").tableFilter({additionalFilterTriggers: [jQuery("#buscador_rapido")]});
    jQuery("#tabla_consulta").colorize({ ignoreHeaders:true }); 
    //jQuery("#tabla_consulta").tablesorter();
    //resaltarPalabra(jQuery("#txtLIB").val());
}

function contarCheckboxes()
{
    
    var cant = 0;
    jQuery(".seleccionado").each(function(){ cant++;});

    return cant;

}

function mensaje(texto){
	var $mensaje=jQuery("<div align='absmiddle' ><p></p></div>").html(texto).dialog({
		autoOpen: true,
		height: 200,
		width: 300,
		modal: true,
		title: "mensaje",
		resizable: false
	});
	setTimeout(function(){$mensaje.dialog("close");},3000);
	
}

function contarCheckboxesEnMostrarEjemplares()
{
    
    var cant = 0;
    jQuery(".row_mostrar_ejemplares").each(function(i,val){ 
        if(val.checked == true){
        cant++;
        }
    });
//     alert("cant"+cant)
    return cant;

}

jQuery(function(){

        
//         jQuery.ajaxSetup({
//             cache: false
//         });
      
        jQuery('#consulta_fichas').dialog(
        {
                bgiframe: true,
                autoOpen: false,
                height: 600,
                width: 800,
                modal: true,
                title: "consulta de fichas"
			
		});
		
        jQuery('#reserva_ejemplares').dialog(
        {
                bgiframe: true,
                autoOpen: false,
                height: 600,
                width: 800,
                modal: true,
                title: "reserva de ejemplares"
		});
        
        jQuery('#consulta_perfil').dialog(
        {
                bgiframe: true,
                autoOpen: false,
                height: 600,
                width: 750,
                modal: true,
                title: "detalles de usuario"
		});
        
        jQuery('#contacto_institucion').dialog(
        {
                bgiframe: true,
                autoOpen: false,
                height: 600,
                width: 800,
                modal: true,
                title: "contacto"
		});
        
        
        
        

        //all hover and click logic for buttons
        jQuery(".fg-button:not(.ui-state-disabled)")
        .hover(
            function(){
                jQuery(this).addClass("ui-state-hover");
            },
            function(){
                jQuery(this).removeClass("ui-state-hover");
            }
        )
        .mousedown(function(){
                jQuery(this).parents('.fg-buttonset-single:first').find(".fg-button.ui-state-active").removeClass("ui-state-active");
                if( jQuery(this).is('.ui-state-active.fg-button-toggleable, .fg-buttonset-multi .ui-state-active') ){
                    jQuery(this).removeClass("ui-state-active"); }
                else { jQuery(this).addClass("ui-state-active"); }
            }
        )
        .mouseup(function(){
                if(! jQuery(this).is('.fg-button-toggleable, .fg-buttonset-single .fg-button, .fg-buttonset-multi .fg-button') ){
                    jQuery(this).removeClass("ui-state-active");
                }
            }
        );
        

});

var html5lightbox_options = {
        watermark: "http://54.83.8.32/images/logo__Red_300px.png",
        watermarklink: "http://www.cali.gov.co/redbibliotecas/"
    };

