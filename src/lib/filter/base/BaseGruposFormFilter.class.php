<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Grupos filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseGruposFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'descripcion'      => new sfWidgetFormFilterInput(),
      'librosporusuario' => new sfWidgetFormFilterInput(),
      'multapordia'      => new sfWidgetFormFilterInput(),
      'diasdeentrega'    => new sfWidgetFormFilterInput(),
      'renovaciones'     => new sfWidgetFormFilterInput(),
      'iniciodevigencia' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'findevigencia'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'descripcion'      => new sfValidatorPass(array('required' => false)),
      'librosporusuario' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'multapordia'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'diasdeentrega'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'renovaciones'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'iniciodevigencia' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'findevigencia'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('grupos_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Grupos';
  }

  public function getFields()
  {
    return array(
      'no'               => 'Number',
      'descripcion'      => 'Text',
      'librosporusuario' => 'Number',
      'multapordia'      => 'Number',
      'diasdeentrega'    => 'Number',
      'renovaciones'     => 'Number',
      'iniciodevigencia' => 'Date',
      'findevigencia'    => 'Date',
    );
  }
}
