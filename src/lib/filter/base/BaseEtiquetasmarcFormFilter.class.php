<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Etiquetasmarc filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseEtiquetasmarcFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'idficha'     => new sfWidgetFormFilterInput(),
      'numetiqueta' => new sfWidgetFormFilterInput(),
      'contenido'   => new sfWidgetFormFilterInput(),
      'posicion'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'idficha'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'numetiqueta' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'contenido'   => new sfValidatorPass(array('required' => false)),
      'posicion'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('etiquetasmarc_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Etiquetasmarc';
  }

  public function getFields()
  {
    return array(
      'idregistro'  => 'Number',
      'idficha'     => 'Number',
      'numetiqueta' => 'Number',
      'contenido'   => 'Text',
      'posicion'    => 'Number',
      'biblioteca'  => 'ForeignKey',
    );
  }
}
