<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Bibliotecas filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseBibliotecasFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'        => new sfWidgetFormFilterInput(),
      'nombrecorto'   => new sfWidgetFormFilterInput(),
      'director'      => new sfWidgetFormFilterInput(),
      'telefono'      => new sfWidgetFormFilterInput(),
      'correo'        => new sfWidgetFormFilterInput(),
      'siabucversion' => new sfWidgetFormFilterInput(),
      'activo'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'        => new sfValidatorPass(array('required' => false)),
      'nombrecorto'   => new sfValidatorPass(array('required' => false)),
      'director'      => new sfValidatorPass(array('required' => false)),
      'telefono'      => new sfValidatorPass(array('required' => false)),
      'correo'        => new sfValidatorPass(array('required' => false)),
      'siabucversion' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'activo'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('bibliotecas_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bibliotecas';
  }

  public function getFields()
  {
    return array(
      'idbiblioteca'  => 'Number',
      'nombre'        => 'Text',
      'nombrecorto'   => 'Text',
      'director'      => 'Text',
      'telefono'      => 'Text',
      'correo'        => 'Text',
      'siabucversion' => 'Number',
      'activo'        => 'Number',
    );
  }
}
