<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Prestamos filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BasePrestamosFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'        => new sfWidgetFormFilterInput(),
      'titulo'        => new sfWidgetFormFilterInput(),
      'autor'         => new sfWidgetFormFilterInput(),
      'clasificacion' => new sfWidgetFormFilterInput(),
      'noescuela'     => new sfWidgetFormFilterInput(),
      'fechasalida'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'fechaentrega'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'renovaciones'  => new sfWidgetFormFilterInput(),
      'analista'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'        => new sfValidatorPass(array('required' => false)),
      'titulo'        => new sfValidatorPass(array('required' => false)),
      'autor'         => new sfValidatorPass(array('required' => false)),
      'clasificacion' => new sfValidatorPass(array('required' => false)),
      'noescuela'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fechasalida'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'fechaentrega'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'renovaciones'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'analista'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('prestamos_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Prestamos';
  }

  public function getFields()
  {
    return array(
      'nocuenta'      => 'Text',
      'numadqui'      => 'Text',
      'nombre'        => 'Text',
      'titulo'        => 'Text',
      'autor'         => 'Text',
      'clasificacion' => 'Text',
      'noescuela'     => 'Number',
      'fechasalida'   => 'Date',
      'fechaentrega'  => 'Date',
      'renovaciones'  => 'Number',
      'analista'      => 'Text',
    );
  }
}
