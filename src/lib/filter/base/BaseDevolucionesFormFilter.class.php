<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Devoluciones filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseDevolucionesFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'fechaentrega'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'noescuela'      => new sfWidgetFormFilterInput(),
      'renovaciones'   => new sfWidgetFormFilterInput(),
      'analista'       => new sfWidgetFormFilterInput(),
      'fechaprestamo'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'fechaaentregar' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'diasretraso'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'fechaentrega'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'noescuela'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'renovaciones'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'analista'       => new sfValidatorPass(array('required' => false)),
      'fechaprestamo'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'fechaaentregar' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'diasretraso'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('devoluciones_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Devoluciones';
  }

  public function getFields()
  {
    return array(
      'nocuenta'       => 'Text',
      'numadqui'       => 'Text',
      'fechaentrega'   => 'Date',
      'noescuela'      => 'Number',
      'renovaciones'   => 'Number',
      'analista'       => 'Text',
      'fechaprestamo'  => 'Date',
      'fechaaentregar' => 'Date',
      'diasretraso'    => 'Number',
    );
  }
}
