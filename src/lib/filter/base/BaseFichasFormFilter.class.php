<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Fichas filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseFichasFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'fecha'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'fechamod'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'datosfijos'    => new sfWidgetFormFilterInput(),
      'etiquetasmarc' => new sfWidgetFormFilterInput(),
      'tipomaterial'  => new sfWidgetFormFilterInput(),
      'isbn'          => new sfWidgetFormFilterInput(),
      'titulo'        => new sfWidgetFormFilterInput(),
      'autor'         => new sfWidgetFormFilterInput(),
      'clasificacion' => new sfWidgetFormFilterInput(),
      'estatus'       => new sfWidgetFormFilterInput(),
      'coleccion_no'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'fecha'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'fechamod'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'datosfijos'    => new sfValidatorPass(array('required' => false)),
      'etiquetasmarc' => new sfValidatorPass(array('required' => false)),
      'tipomaterial'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'isbn'          => new sfValidatorPass(array('required' => false)),
      'titulo'        => new sfValidatorPass(array('required' => false)),
      'autor'         => new sfValidatorPass(array('required' => false)),
      'clasificacion' => new sfValidatorPass(array('required' => false)),
      'estatus'       => new sfValidatorPass(array('required' => false)),
      'coleccion_no'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('fichas_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Fichas';
  }

  public function getFields()
  {
    return array(
      'ficha_no'      => 'Number',
      'fecha'         => 'Date',
      'fechamod'      => 'Date',
      'datosfijos'    => 'Text',
      'etiquetasmarc' => 'Text',
      'tipomaterial'  => 'Number',
      'isbn'          => 'Text',
      'titulo'        => 'Text',
      'autor'         => 'Text',
      'clasificacion' => 'Text',
      'estatus'       => 'Text',
      'coleccion_no'  => 'Number',
    );
  }
}
