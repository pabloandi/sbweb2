<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Escuelas filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseEscuelasFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'       => new sfWidgetFormFilterInput(),
      'nombrecorto'  => new sfWidgetFormFilterInput(),
      'responsable'  => new sfWidgetFormFilterInput(),
      'email'        => new sfWidgetFormFilterInput(),
      'nobiblioteca' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'       => new sfValidatorPass(array('required' => false)),
      'nombrecorto'  => new sfValidatorPass(array('required' => false)),
      'responsable'  => new sfValidatorPass(array('required' => false)),
      'email'        => new sfValidatorPass(array('required' => false)),
      'nobiblioteca' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('escuelas_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Escuelas';
  }

  public function getFields()
  {
    return array(
      'no'           => 'Number',
      'nombre'       => 'Text',
      'nombrecorto'  => 'Text',
      'responsable'  => 'Text',
      'email'        => 'Text',
      'nobiblioteca' => 'Number',
    );
  }
}
