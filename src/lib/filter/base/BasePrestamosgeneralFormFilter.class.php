<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Prestamosgeneral filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BasePrestamosgeneralFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'        => new sfWidgetFormFilterInput(),
      'titulo'        => new sfWidgetFormFilterInput(),
      'autor'         => new sfWidgetFormFilterInput(),
      'clasificacion' => new sfWidgetFormFilterInput(),
      'fechasalida'   => new sfWidgetFormFilterInput(),
      'fechaentrega'  => new sfWidgetFormFilterInput(),
      'tipoprestamo'  => new sfWidgetFormFilterInput(),
      'biblioteca'    => new sfWidgetFormPropelChoice(array('model' => 'Bibliotecas', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'nombre'        => new sfValidatorPass(array('required' => false)),
      'titulo'        => new sfValidatorPass(array('required' => false)),
      'autor'         => new sfValidatorPass(array('required' => false)),
      'clasificacion' => new sfValidatorPass(array('required' => false)),
      'fechasalida'   => new sfValidatorPass(array('required' => false)),
      'fechaentrega'  => new sfValidatorPass(array('required' => false)),
      'tipoprestamo'  => new sfValidatorPass(array('required' => false)),
      'biblioteca'    => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Bibliotecas', 'column' => 'idbiblioteca')),
    ));

    $this->widgetSchema->setNameFormat('prestamosgeneral_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Prestamosgeneral';
  }

  public function getFields()
  {
    return array(
      'nocuenta'      => 'Text',
      'numadqui'      => 'Text',
      'nombre'        => 'Text',
      'titulo'        => 'Text',
      'autor'         => 'Text',
      'clasificacion' => 'Text',
      'fechasalida'   => 'Text',
      'fechaentrega'  => 'Text',
      'tipoprestamo'  => 'Text',
      'biblioteca'    => 'ForeignKey',
    );
  }
}
