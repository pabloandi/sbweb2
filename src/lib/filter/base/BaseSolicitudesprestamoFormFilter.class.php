<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Solicitudesprestamo filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseSolicitudesprestamoFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nocuenta'      => new sfWidgetFormFilterInput(),
      'numadqui'      => new sfWidgetFormFilterInput(),
      'usuario'       => new sfWidgetFormFilterInput(),
      'titulo'        => new sfWidgetFormFilterInput(),
      'autor'         => new sfWidgetFormFilterInput(),
      'clasificacion' => new sfWidgetFormFilterInput(),
      'estado_id'     => new sfWidgetFormPropelChoice(array('model' => 'Estado', 'add_empty' => true)),
      'created_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'updated_at'    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'nocuenta'      => new sfValidatorPass(array('required' => false)),
      'numadqui'      => new sfValidatorPass(array('required' => false)),
      'usuario'       => new sfValidatorPass(array('required' => false)),
      'titulo'        => new sfValidatorPass(array('required' => false)),
      'autor'         => new sfValidatorPass(array('required' => false)),
      'clasificacion' => new sfValidatorPass(array('required' => false)),
      'estado_id'     => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Estado', 'column' => 'id')),
      'created_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'updated_at'    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('solicitudesprestamo_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Solicitudesprestamo';
  }

  public function getFields()
  {
    return array(
      'id'            => 'Number',
      'nocuenta'      => 'Text',
      'numadqui'      => 'Text',
      'usuario'       => 'Text',
      'titulo'        => 'Text',
      'autor'         => 'Text',
      'clasificacion' => 'Text',
      'estado_id'     => 'ForeignKey',
      'created_at'    => 'Date',
      'updated_at'    => 'Date',
    );
  }
}
