<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Colecciones filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseColeccionesFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'               => new sfWidgetFormFilterInput(),
      'descripcion'          => new sfWidgetFormFilterInput(),
      'observaciones'        => new sfWidgetFormFilterInput(),
      'tipomaterial'         => new sfWidgetFormFilterInput(),
      'fuentefinanciamiento' => new sfWidgetFormFilterInput(),
      'atributos'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'               => new sfValidatorPass(array('required' => false)),
      'descripcion'          => new sfValidatorPass(array('required' => false)),
      'observaciones'        => new sfValidatorPass(array('required' => false)),
      'tipomaterial'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fuentefinanciamiento' => new sfValidatorPass(array('required' => false)),
      'atributos'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('colecciones_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Colecciones';
  }

  public function getFields()
  {
    return array(
      'coleccion_no'         => 'Number',
      'nombre'               => 'Text',
      'descripcion'          => 'Text',
      'observaciones'        => 'Text',
      'tipomaterial'         => 'Number',
      'fuentefinanciamiento' => 'Text',
      'atributos'            => 'Number',
    );
  }
}
