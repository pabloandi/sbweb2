<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Usuariosgeneral filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseUsuariosgeneralFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'       => new sfWidgetFormFilterInput(),
      'nogrupo'      => new sfWidgetFormFilterInput(),
      'noescuela'    => new sfWidgetFormFilterInput(),
      'email'        => new sfWidgetFormFilterInput(),
      'domicilio'    => new sfWidgetFormFilterInput(),
      'colonia'      => new sfWidgetFormFilterInput(),
      'ciudadestado' => new sfWidgetFormFilterInput(),
      'telefono'     => new sfWidgetFormFilterInput(),
      'notas'        => new sfWidgetFormFilterInput(),
      'multa'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'       => new sfValidatorPass(array('required' => false)),
      'nogrupo'      => new sfValidatorPass(array('required' => false)),
      'noescuela'    => new sfValidatorPass(array('required' => false)),
      'email'        => new sfValidatorPass(array('required' => false)),
      'domicilio'    => new sfValidatorPass(array('required' => false)),
      'colonia'      => new sfValidatorPass(array('required' => false)),
      'ciudadestado' => new sfValidatorPass(array('required' => false)),
      'telefono'     => new sfValidatorPass(array('required' => false)),
      'notas'        => new sfValidatorPass(array('required' => false)),
      'multa'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('usuariosgeneral_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Usuariosgeneral';
  }

  public function getFields()
  {
    return array(
      'nocuenta'     => 'Text',
      'nombre'       => 'Text',
      'nogrupo'      => 'Text',
      'noescuela'    => 'Text',
      'email'        => 'Text',
      'domicilio'    => 'Text',
      'colonia'      => 'Text',
      'ciudadestado' => 'Text',
      'telefono'     => 'Text',
      'notas'        => 'Text',
      'multa'        => 'Text',
    );
  }
}
