<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Ejemplaresgeneral filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseEjemplaresgeneralFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'ano'        => new sfWidgetFormFilterInput(),
      'volumen'    => new sfWidgetFormFilterInput(),
      'numero'     => new sfWidgetFormFilterInput(),
      'ejemplar'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'ano'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'volumen'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'numero'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ejemplar'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ejemplaresgeneral_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ejemplaresgeneral';
  }

  public function getFields()
  {
    return array(
      'ficha_no'   => 'Text',
      'numadqui'   => 'Text',
      'biblioteca' => 'ForeignKey',
      'ano'        => 'Number',
      'volumen'    => 'Number',
      'numero'     => 'Number',
      'ejemplar'   => 'Number',
    );
  }
}
