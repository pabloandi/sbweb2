<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Multaspendientes filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseMultaspendientesFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nocuenta'        => new sfWidgetFormFilterInput(),
      'nombre'          => new sfWidgetFormFilterInput(),
      'escuela'         => new sfWidgetFormFilterInput(),
      'fechadevolucion' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'monto'           => new sfWidgetFormFilterInput(),
      'observaciones'   => new sfWidgetFormFilterInput(),
      'idcapturista'    => new sfWidgetFormFilterInput(),
      'idbloqueo'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nocuenta'        => new sfValidatorPass(array('required' => false)),
      'nombre'          => new sfValidatorPass(array('required' => false)),
      'escuela'         => new sfValidatorPass(array('required' => false)),
      'fechadevolucion' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'monto'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'observaciones'   => new sfValidatorPass(array('required' => false)),
      'idcapturista'    => new sfValidatorPass(array('required' => false)),
      'idbloqueo'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('multaspendientes_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Multaspendientes';
  }

  public function getFields()
  {
    return array(
      'idmulta'         => 'Number',
      'nocuenta'        => 'Text',
      'nombre'          => 'Text',
      'escuela'         => 'Text',
      'fechadevolucion' => 'Date',
      'monto'           => 'Number',
      'observaciones'   => 'Text',
      'idcapturista'    => 'Text',
      'idbloqueo'       => 'Number',
      'biblioteca'      => 'ForeignKey',
    );
  }
}
