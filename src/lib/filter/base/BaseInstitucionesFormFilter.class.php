<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Instituciones filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseInstitucionesFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'        => new sfWidgetFormFilterInput(),
      'representante' => new sfWidgetFormFilterInput(),
      'direccion'     => new sfWidgetFormFilterInput(),
      'ciudad'        => new sfWidgetFormFilterInput(),
      'telefonofax'   => new sfWidgetFormFilterInput(),
      'email'         => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'        => new sfValidatorPass(array('required' => false)),
      'representante' => new sfValidatorPass(array('required' => false)),
      'direccion'     => new sfValidatorPass(array('required' => false)),
      'ciudad'        => new sfValidatorPass(array('required' => false)),
      'telefonofax'   => new sfValidatorPass(array('required' => false)),
      'email'         => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('instituciones_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Instituciones';
  }

  public function getFields()
  {
    return array(
      'no'            => 'Number',
      'nombre'        => 'Text',
      'representante' => 'Text',
      'direccion'     => 'Text',
      'ciudad'        => 'Text',
      'telefonofax'   => 'Text',
      'email'         => 'Text',
    );
  }
}
