<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Fichasgeneral filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseFichasgeneralFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'etiquetasmarc'    => new sfWidgetFormFilterInput(),
      'titulo'           => new sfWidgetFormFilterInput(),
      'autor'            => new sfWidgetFormFilterInput(),
      'clasificacion'    => new sfWidgetFormFilterInput(),
      'isbn'             => new sfWidgetFormFilterInput(),
      'fechapublicacion' => new sfWidgetFormFilterInput(),
      'tipomaterial'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'etiquetasmarc'    => new sfValidatorPass(array('required' => false)),
      'titulo'           => new sfValidatorPass(array('required' => false)),
      'autor'            => new sfValidatorPass(array('required' => false)),
      'clasificacion'    => new sfValidatorPass(array('required' => false)),
      'isbn'             => new sfValidatorPass(array('required' => false)),
      'fechapublicacion' => new sfValidatorPass(array('required' => false)),
      'tipomaterial'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fichasgeneral_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Fichasgeneral';
  }

  public function getFields()
  {
    return array(
      'ficha_no'         => 'Text',
      'etiquetasmarc'    => 'Text',
      'titulo'           => 'Text',
      'autor'            => 'Text',
      'clasificacion'    => 'Text',
      'isbn'             => 'Text',
      'fechapublicacion' => 'Text',
      'tipomaterial'     => 'Text',
      'biblioteca'       => 'ForeignKey',
    );
  }
}
