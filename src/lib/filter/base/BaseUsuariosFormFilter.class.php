<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Usuarios filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseUsuariosFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nombre'       => new sfWidgetFormFilterInput(),
      'nogrupo'      => new sfWidgetFormFilterInput(),
      'noescuela'    => new sfWidgetFormFilterInput(),
      'email'        => new sfWidgetFormFilterInput(),
      'domicilio'    => new sfWidgetFormFilterInput(),
      'colonia'      => new sfWidgetFormFilterInput(),
      'ciudadestado' => new sfWidgetFormFilterInput(),
      'codigopostal' => new sfWidgetFormFilterInput(),
      'telefono'     => new sfWidgetFormFilterInput(),
      'fotografia'   => new sfWidgetFormFilterInput(),
      'notas'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nombre'       => new sfValidatorPass(array('required' => false)),
      'nogrupo'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'noescuela'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'email'        => new sfValidatorPass(array('required' => false)),
      'domicilio'    => new sfValidatorPass(array('required' => false)),
      'colonia'      => new sfValidatorPass(array('required' => false)),
      'ciudadestado' => new sfValidatorPass(array('required' => false)),
      'codigopostal' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'telefono'     => new sfValidatorPass(array('required' => false)),
      'fotografia'   => new sfValidatorPass(array('required' => false)),
      'notas'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('usuarios_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Usuarios';
  }

  public function getFields()
  {
    return array(
      'nocuenta'     => 'Text',
      'nombre'       => 'Text',
      'nogrupo'      => 'Number',
      'noescuela'    => 'Number',
      'email'        => 'Text',
      'domicilio'    => 'Text',
      'colonia'      => 'Text',
      'ciudadestado' => 'Text',
      'codigopostal' => 'Number',
      'telefono'     => 'Text',
      'fotografia'   => 'Text',
      'notas'        => 'Text',
    );
  }
}
