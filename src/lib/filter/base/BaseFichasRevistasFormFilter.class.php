<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * FichasRevistas filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseFichasRevistasFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'periodicidad'  => new sfWidgetFormFilterInput(),
      'fecha'         => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'tipoadquis'    => new sfWidgetFormFilterInput(),
      'proveedor'     => new sfWidgetFormFilterInput(),
      'suscripcion'   => new sfWidgetFormFilterInput(),
      'precio'        => new sfWidgetFormFilterInput(),
      'etiquetasmarc' => new sfWidgetFormFilterInput(),
      'issn'          => new sfWidgetFormFilterInput(),
      'activo'        => new sfWidgetFormFilterInput(),
      'institucion'   => new sfWidgetFormFilterInput(),
      'titulo'        => new sfWidgetFormFilterInput(),
      'clasificacion' => new sfWidgetFormFilterInput(),
      'idioma'        => new sfWidgetFormFilterInput(),
      'pais'          => new sfWidgetFormFilterInput(),
      'editorial'     => new sfWidgetFormFilterInput(),
      'fechasusc'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
    ));

    $this->setValidators(array(
      'periodicidad'  => new sfValidatorPass(array('required' => false)),
      'fecha'         => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'tipoadquis'    => new sfValidatorPass(array('required' => false)),
      'proveedor'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'suscripcion'   => new sfValidatorPass(array('required' => false)),
      'precio'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'etiquetasmarc' => new sfValidatorPass(array('required' => false)),
      'issn'          => new sfValidatorPass(array('required' => false)),
      'activo'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'institucion'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'titulo'        => new sfValidatorPass(array('required' => false)),
      'clasificacion' => new sfValidatorPass(array('required' => false)),
      'idioma'        => new sfValidatorPass(array('required' => false)),
      'pais'          => new sfValidatorPass(array('required' => false)),
      'editorial'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fechasusc'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
    ));

    $this->widgetSchema->setNameFormat('fichas_revistas_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'FichasRevistas';
  }

  public function getFields()
  {
    return array(
      'ficha_no'      => 'Number',
      'periodicidad'  => 'Text',
      'fecha'         => 'Date',
      'tipoadquis'    => 'Text',
      'proveedor'     => 'Number',
      'suscripcion'   => 'Text',
      'precio'        => 'Number',
      'etiquetasmarc' => 'Text',
      'issn'          => 'Text',
      'activo'        => 'Number',
      'institucion'   => 'Number',
      'titulo'        => 'Text',
      'clasificacion' => 'Text',
      'idioma'        => 'Text',
      'pais'          => 'Text',
      'editorial'     => 'Number',
      'fechasusc'     => 'Date',
    );
  }
}
