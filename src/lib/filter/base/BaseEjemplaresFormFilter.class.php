<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Ejemplares filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseEjemplaresFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'fechaingreso' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'biblioteca'   => new sfWidgetFormPropelChoice(array('model' => 'Bibliotecas', 'add_empty' => true)),
      'volumen'      => new sfWidgetFormFilterInput(),
      'ejemplar'     => new sfWidgetFormFilterInput(),
      'tomo'         => new sfWidgetFormFilterInput(),
      'accesible'    => new sfWidgetFormFilterInput(),
      'noescuela'    => new sfWidgetFormFilterInput(),
      'fechamod'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'analista'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'fechaingreso' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'biblioteca'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Bibliotecas', 'column' => 'no')),
      'volumen'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ejemplar'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'tomo'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'accesible'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'noescuela'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fechamod'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'analista'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ejemplares_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ejemplares';
  }

  public function getFields()
  {
    return array(
      'ficha_no'     => 'ForeignKey',
      'numadqui'     => 'Text',
      'fechaingreso' => 'Date',
      'biblioteca'   => 'ForeignKey',
      'volumen'      => 'Number',
      'ejemplar'     => 'Number',
      'tomo'         => 'Number',
      'accesible'    => 'Number',
      'noescuela'    => 'Number',
      'fechamod'     => 'Date',
      'analista'     => 'Text',
    );
  }
}
