<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * Reservados filter form base class.
 *
 * @package    sbweb
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseReservadosFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'fechainicial' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'fechafinal'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'nocuenta'     => new sfWidgetFormFilterInput(),
      'biblioteca'   => new sfWidgetFormPropelChoice(array('model' => 'Bibliotecas', 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'fechainicial' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'fechafinal'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'nocuenta'     => new sfValidatorPass(array('required' => false)),
      'biblioteca'   => new sfValidatorPropelChoice(array('required' => false, 'model' => 'Bibliotecas', 'column' => 'idbiblioteca')),
    ));

    $this->widgetSchema->setNameFormat('reservados_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Reservados';
  }

  public function getFields()
  {
    return array(
      'numadqui'     => 'Text',
      'fechainicial' => 'Date',
      'fechafinal'   => 'Date',
      'nocuenta'     => 'Text',
      'biblioteca'   => 'ForeignKey',
    );
  }
}
