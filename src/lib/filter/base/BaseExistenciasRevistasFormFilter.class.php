<?php

require_once(sfConfig::get('sf_lib_dir').'/filter/base/BaseFormFilterPropel.class.php');

/**
 * ExistenciasRevistas filter form base class.
 *
 * @package    sf_sandbox
 * @subpackage filter
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormFilterGeneratedTemplate.php 13459 2008-11-28 14:48:12Z fabien $
 */
class BaseExistenciasRevistasFormFilter extends BaseFormFilterPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'fecharecepcion' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'biblioteca'     => new sfWidgetFormFilterInput(),
      'ano'            => new sfWidgetFormFilterInput(),
      'volumen'        => new sfWidgetFormFilterInput(),
      'numero'         => new sfWidgetFormFilterInput(),
      'fecha'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => true)),
      'numcopia'       => new sfWidgetFormFilterInput(),
      'notanumero'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'fecharecepcion' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'biblioteca'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ano'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'volumen'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'numero'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'fecha'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDate(array('required' => false)))),
      'numcopia'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'notanumero'     => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('existencias_revistas_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'ExistenciasRevistas';
  }

  public function getFields()
  {
    return array(
      'ficha_no'       => 'Number',
      'fecharecepcion' => 'Date',
      'numadqui'       => 'Text',
      'biblioteca'     => 'Number',
      'ano'            => 'Number',
      'volumen'        => 'Number',
      'numero'         => 'Number',
      'fecha'          => 'Date',
      'numcopia'       => 'Number',
      'notanumero'     => 'Text',
    );
  }
}
