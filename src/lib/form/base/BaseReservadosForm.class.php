<?php

/**
 * Reservados form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseReservadosForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'numadqui'     => new sfWidgetFormInputHidden(),
      'fechainicial' => new sfWidgetFormDateTime(),
      'fechafinal'   => new sfWidgetFormDateTime(),
      'nocuenta'     => new sfWidgetFormInput(),
      'biblioteca'   => new sfWidgetFormPropelChoice(array('model' => 'Bibliotecas', 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'numadqui'     => new sfValidatorPropelChoice(array('model' => 'Reservados', 'column' => 'numadqui', 'required' => false)),
      'fechainicial' => new sfValidatorDateTime(array('required' => false)),
      'fechafinal'   => new sfValidatorDateTime(array('required' => false)),
      'nocuenta'     => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'biblioteca'   => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca')),
    ));

    $this->widgetSchema->setNameFormat('reservados[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Reservados';
  }


}
