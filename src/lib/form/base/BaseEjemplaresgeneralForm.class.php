<?php

/**
 * Ejemplaresgeneral form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseEjemplaresgeneralForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'ficha_no'   => new sfWidgetFormInputHidden(),
      'numadqui'   => new sfWidgetFormInputHidden(),
      'biblioteca' => new sfWidgetFormInputHidden(),
      'ano'        => new sfWidgetFormInput(),
      'volumen'    => new sfWidgetFormInput(),
      'numero'     => new sfWidgetFormInput(),
      'ejemplar'   => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'ficha_no'   => new sfValidatorPropelChoice(array('model' => 'Ejemplaresgeneral', 'column' => 'ficha_no', 'required' => false)),
      'numadqui'   => new sfValidatorPropelChoice(array('model' => 'Ejemplaresgeneral', 'column' => 'numadqui', 'required' => false)),
      'biblioteca' => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca', 'required' => false)),
      'ano'        => new sfValidatorInteger(array('required' => false)),
      'volumen'    => new sfValidatorInteger(array('required' => false)),
      'numero'     => new sfValidatorInteger(array('required' => false)),
      'ejemplar'   => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ejemplaresgeneral[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Ejemplaresgeneral';
  }


}
