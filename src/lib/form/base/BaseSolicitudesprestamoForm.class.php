<?php

/**
 * Solicitudesprestamo form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseSolicitudesprestamoForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'nocuenta'      => new sfWidgetFormInput(),
      'numadqui'      => new sfWidgetFormInput(),
      'usuario'       => new sfWidgetFormInput(),
      'titulo'        => new sfWidgetFormInput(),
      'autor'         => new sfWidgetFormInput(),
      'clasificacion' => new sfWidgetFormInput(),
      'estado_id'     => new sfWidgetFormPropelChoice(array('model' => 'Estado', 'add_empty' => true)),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorPropelChoice(array('model' => 'Solicitudesprestamo', 'column' => 'id', 'required' => false)),
      'nocuenta'      => new sfValidatorString(array('max_length' => 15)),
      'numadqui'      => new sfValidatorString(array('max_length' => 15)),
      'usuario'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'titulo'        => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'autor'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'clasificacion' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'estado_id'     => new sfValidatorPropelChoice(array('model' => 'Estado', 'column' => 'id', 'required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('solicitudesprestamo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Solicitudesprestamo';
  }


}
