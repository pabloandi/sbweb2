<?php

/**
 * Multaspendientes form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseMultaspendientesForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'idmulta'         => new sfWidgetFormInputHidden(),
      'nocuenta'        => new sfWidgetFormInput(),
      'nombre'          => new sfWidgetFormInput(),
      'escuela'         => new sfWidgetFormInput(),
      'fechadevolucion' => new sfWidgetFormDateTime(),
      'monto'           => new sfWidgetFormInput(),
      'observaciones'   => new sfWidgetFormTextarea(),
      'idcapturista'    => new sfWidgetFormInput(),
      'idbloqueo'       => new sfWidgetFormInput(),
      'biblioteca'      => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'idmulta'         => new sfValidatorPropelChoice(array('model' => 'Multaspendientes', 'column' => 'idmulta', 'required' => false)),
      'nocuenta'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'nombre'          => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'escuela'         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'fechadevolucion' => new sfValidatorDateTime(array('required' => false)),
      'monto'           => new sfValidatorInteger(array('required' => false)),
      'observaciones'   => new sfValidatorString(array('required' => false)),
      'idcapturista'    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'idbloqueo'       => new sfValidatorInteger(array('required' => false)),
      'biblioteca'      => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('multaspendientes[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Multaspendientes';
  }


}
