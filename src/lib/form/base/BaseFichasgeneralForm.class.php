<?php

/**
 * Fichasgeneral form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseFichasgeneralForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'ficha_no'         => new sfWidgetFormInputHidden(),
      'etiquetasmarc'    => new sfWidgetFormTextarea(),
      'titulo'           => new sfWidgetFormTextarea(),
      'autor'            => new sfWidgetFormTextarea(),
      'clasificacion'    => new sfWidgetFormTextarea(),
      'isbn'             => new sfWidgetFormInput(),
      'fechapublicacion' => new sfWidgetFormInput(),
      'tipomaterial'     => new sfWidgetFormInput(),
      'biblioteca'       => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'ficha_no'         => new sfValidatorPropelChoice(array('model' => 'Fichasgeneral', 'column' => 'ficha_no', 'required' => false)),
      'etiquetasmarc'    => new sfValidatorString(array('required' => false)),
      'titulo'           => new sfValidatorString(array('required' => false)),
      'autor'            => new sfValidatorString(array('required' => false)),
      'clasificacion'    => new sfValidatorString(array('required' => false)),
      'isbn'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'fechapublicacion' => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'tipomaterial'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'biblioteca'       => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fichasgeneral[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Fichasgeneral';
  }


}
