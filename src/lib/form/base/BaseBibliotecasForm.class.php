<?php

/**
 * Bibliotecas form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseBibliotecasForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'idbiblioteca'  => new sfWidgetFormInputHidden(),
      'nombre'        => new sfWidgetFormInput(),
      'nombrecorto'   => new sfWidgetFormInput(),
      'director'      => new sfWidgetFormInput(),
      'telefono'      => new sfWidgetFormInput(),
      'correo'        => new sfWidgetFormInput(),
      'siabucversion' => new sfWidgetFormInput(),
      'activo'        => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'idbiblioteca'  => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca', 'required' => false)),
      'nombre'        => new sfValidatorString(array('max_length' => 250)),
      'nombrecorto'   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'director'      => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'telefono'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'correo'        => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'siabucversion' => new sfValidatorInteger(array('required' => false)),
      'activo'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bibliotecas[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bibliotecas';
  }


}
