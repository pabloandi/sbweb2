<?php

/**
 * Buzon form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseBuzonForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'nombre'     => new sfWidgetFormInput(),
      'correo'     => new sfWidgetFormInput(),
      'asunto'     => new sfWidgetFormInput(),
      'mensaje'    => new sfWidgetFormTextarea(),
      'biblioteca' => new sfWidgetFormInput(),
      'recibido'   => new sfWidgetFormInputCheckbox(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorPropelChoice(array('model' => 'Buzon', 'column' => 'id', 'required' => false)),
      'nombre'     => new sfValidatorString(array('max_length' => 50)),
      'correo'     => new sfValidatorString(array('max_length' => 50)),
      'asunto'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'mensaje'    => new sfValidatorString(array('required' => false)),
      'biblioteca' => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'recibido'   => new sfValidatorBoolean(array('required' => false)),
      'created_at' => new sfValidatorDateTime(array('required' => false)),
      'updated_at' => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('buzon[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Buzon';
  }


}
