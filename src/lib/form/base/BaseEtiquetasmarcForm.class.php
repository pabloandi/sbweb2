<?php

/**
 * Etiquetasmarc form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseEtiquetasmarcForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'idregistro'  => new sfWidgetFormInputHidden(),
      'idficha'     => new sfWidgetFormInput(),
      'numetiqueta' => new sfWidgetFormInput(),
      'contenido'   => new sfWidgetFormTextarea(),
      'posicion'    => new sfWidgetFormInput(),
      'biblioteca'  => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'idregistro'  => new sfValidatorPropelChoice(array('model' => 'Etiquetasmarc', 'column' => 'idregistro', 'required' => false)),
      'idficha'     => new sfValidatorInteger(array('required' => false)),
      'numetiqueta' => new sfValidatorInteger(array('required' => false)),
      'contenido'   => new sfValidatorString(array('required' => false)),
      'posicion'    => new sfValidatorInteger(array('required' => false)),
      'biblioteca'  => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('etiquetasmarc[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Etiquetasmarc';
  }


}
