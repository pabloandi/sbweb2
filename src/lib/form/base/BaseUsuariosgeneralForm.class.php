<?php

/**
 * Usuariosgeneral form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BaseUsuariosgeneralForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nocuenta'     => new sfWidgetFormInputHidden(),
      'nombre'       => new sfWidgetFormInput(),
      'nogrupo'      => new sfWidgetFormInput(),
      'noescuela'    => new sfWidgetFormInput(),
      'email'        => new sfWidgetFormInput(),
      'domicilio'    => new sfWidgetFormInput(),
      'colonia'      => new sfWidgetFormInput(),
      'ciudadestado' => new sfWidgetFormInput(),
      'telefono'     => new sfWidgetFormInput(),
      'notas'        => new sfWidgetFormInput(),
      'multa'        => new sfWidgetFormInput(),
    ));

    $this->setValidators(array(
      'nocuenta'     => new sfValidatorPropelChoice(array('model' => 'Usuariosgeneral', 'column' => 'nocuenta', 'required' => false)),
      'nombre'       => new sfValidatorString(array('max_length' => 50)),
      'nogrupo'      => new sfValidatorString(array('max_length' => 50)),
      'noescuela'    => new sfValidatorString(array('max_length' => 250)),
      'email'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'domicilio'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'colonia'      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'ciudadestado' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'telefono'     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'notas'        => new sfValidatorString(array('max_length' => 120, 'required' => false)),
      'multa'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('usuariosgeneral[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Usuariosgeneral';
  }


}
