<?php

/**
 * Prestamosgeneral form base class.
 *
 * @package    sbweb
 * @subpackage form
 * @author     websoft
 * @version    SVN: $Id: sfPropelFormGeneratedTemplate.php 12815 2008-11-09 10:43:58Z fabien $
 */
class BasePrestamosgeneralForm extends BaseFormPropel
{
  public function setup()
  {
    $this->setWidgets(array(
      'nocuenta'      => new sfWidgetFormInputHidden(),
      'numadqui'      => new sfWidgetFormInputHidden(),
      'nombre'        => new sfWidgetFormInput(),
      'titulo'        => new sfWidgetFormInput(),
      'autor'         => new sfWidgetFormInput(),
      'clasificacion' => new sfWidgetFormInput(),
      'fechasalida'   => new sfWidgetFormInput(),
      'fechaentrega'  => new sfWidgetFormInput(),
      'tipoprestamo'  => new sfWidgetFormInput(),
      'biblioteca'    => new sfWidgetFormPropelChoice(array('model' => 'Bibliotecas', 'add_empty' => false)),
    ));

    $this->setValidators(array(
      'nocuenta'      => new sfValidatorPropelChoice(array('model' => 'Prestamosgeneral', 'column' => 'nocuenta', 'required' => false)),
      'numadqui'      => new sfValidatorPropelChoice(array('model' => 'Prestamosgeneral', 'column' => 'numadqui', 'required' => false)),
      'nombre'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'titulo'        => new sfValidatorString(array('max_length' => 60, 'required' => false)),
      'autor'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'clasificacion' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'fechasalida'   => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'fechaentrega'  => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'tipoprestamo'  => new sfValidatorString(array('max_length' => 16)),
      'biblioteca'    => new sfValidatorPropelChoice(array('model' => 'Bibliotecas', 'column' => 'idbiblioteca')),
    ));

    $this->widgetSchema->setNameFormat('prestamosgeneral[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    parent::setup();
  }

  public function getModelName()
  {
    return 'Prestamosgeneral';
  }


}
