<?php

class Ejemplaresgeneral extends BaseEjemplaresgeneral
{
    public function getEstado()
    {
        list($ficha_no,$num_adqui,$biblioteca)=$this->getPrimaryKey();
        
        return EjemplaresgeneralPeer::getDisponibilidad($num_adqui,$biblioteca);
    }
}
