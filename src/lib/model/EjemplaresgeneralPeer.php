<?php

class EjemplaresgeneralPeer extends BaseEjemplaresgeneralPeer
{
    public static function getDisponibilidad($num_adqui,$biblioteca)
    {
        $conexion = Propel::getConnection();
        
        /* Prestamo interno y Prestamo */
        
        $consulta = "SELECT %s FROM %s WHERE NumAdqui='%s' AND Biblioteca = %s";
        $consulta = sprintf($consulta, "TipoPrestamo,FechaEntrega", "`PrestamosGeneral`",$num_adqui,$biblioteca);
        $sentencia = $conexion->prepare($consulta);
        $sentencia->execute();
        
		if($sentencia->rowCount()>0)
        {
			
            $resultset = $sentencia->fetch(PDO::FETCH_ASSOC);
			
            if($resultset['TipoPrestamo']=='Prestamo interno')
                return 'Prestamo en sala';
            elseif($resultset['TipoPrestamo']=="Prestamo")
            {
				$fecha=$resultset['FechaEntrega'];
                return "En Prestamo ($fecha)";
            }
        }
        
        /* Reservados */
        $consulta = "SELECT %s FROM %s WHERE NumAdqui='%s' AND Biblioteca = %s";
        $consulta = sprintf($consulta, "NumAdqui","`Reservados`",$num_adqui,$biblioteca);
        $sentencia = $conexion->prepare($consulta);
        $sentencia->execute();
        
        if($sentencia->rowCount()>0)
            return "Reservado";
        
        
        return 'Disponible';
        
    }
}
