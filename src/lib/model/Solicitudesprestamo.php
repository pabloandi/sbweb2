<?php

class Solicitudesprestamo extends BaseSolicitudesprestamo
{
	public function getFechaPublicacion(){
		$ejemplar = $this->getEjemplar();

		if($ejemplar){
			$c = new Criteria();
			$c->add(FichasgeneralPeer::FICHA_NO, $ejemplar->getFichaNo());
			$ficha = FichasgeneralPeer::doSelectOne($c);

			return ($ficha) ? $ficha->getFechapublicacion() : null;

		}
	}


	private function getEjemplar()
	{
		if ($this->numadqui !== null) {
			$c = new Criteria();
			$c->add(EjemplaresgeneralPeer::NUMADQUI, $this->getNumadqui());
			$ejemplar = EjemplaresgeneralPeer::doSelectOne($c);

			return ($ejemplar) ? $ejemplar : null;

		}
		else {
			return null;
		}

	}

	public function getBiblioteca(){
		$ejemplar = $this->getEjemplar();

		return ($ejemplar) ? $ejemplar->getBibliotecas()->getNombre() : null;


	}
}
