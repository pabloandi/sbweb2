<?php



class EtiquetasmarcPeer extends BaseEtiquetasmarcPeer {

    public static function getEtiquetasMarc($ficha,$biblioteca=1)
    {
        $c=new Criteria();
        $c->add(self::IDFICHA,substr($ficha->getFichaNo(),1));
	$c->addAscendingOrderByColumn(self::IDREGISTRO)->addAscendingOrderByColumn(self::IDFICHA)->addAscendingOrderByColumn(self::NUMETIQUETA)->addAscendingOrderByColumn(self::POSICION);
        //$c->add(self::BIBLIOTECA,$biblioteca);
        
        
        $datos=array();
        $etiquetas=self::doSelect($c);
        

        //$etiquetas = DbFinder::from('Etiquetasmarc', Propel::getConnection($biblioteca))->where('Idficha',substr($ficha->getPrimaryKey())->find();

        $i=0;
        
        $datos['000 Fecha Publicación']=$ficha->getFechaPublicacion();
        
        $a=$ficha->getPrimaryKey();
  

        if($a[0]=="A")
        {
            $datos["0000Título Revista"]=substr($ficha->getEtiquetasMarc(),4,strpos($ficha->getEtiquetasMarc(),'|245')-4);
        }
        
        $ficha=$ficha->getPrimaryKey();


        $tipo=$ficha[0];
	$titulo="";
        
        //die(var_dump($etiquetas));
        foreach($etiquetas as $actual)
        {
            $numero=$actual->getNumetiqueta();
            $valor=$actual->getContenido();
	    $posicion=$actual->getPosicion();

            
            if($numero=="000")
                $datos["01$i Título Revista"]=$valor;
            else if($numero=="020")
            {
                if($tipo=="L")
                    $datos["02$i ISBN"]=$valor;
                else if($tipo=="R")
                    $datos["02$i ISSN"]=$valor;
            }
            else if($numero=="040")
               $datos["03$i Fuente de catalogación"]=$valor;
            else if($numero=="041")
               $datos["04$i Código de lengua"]=$valor;
            else if($numero=="043")
               $datos["05$i Código de área geográfica"]=$valor;
            else if($numero=="045")
                $datos["06$i Cronológico ó Fecha/Hora"]=$valor;
            else if($numero=="050")
            {
                if($tipo=="R")
                    $datos["07$i Signatura topográfica"]=$valor;
                else
                    $datos["07$i Clasificacion LC"]=$valor;
            }
            else if($numero=="082")
               $datos["08$i Clasificacion Dewey"]=$valor;
            else if($numero=="090")
               $datos["09$i Clasificacion a nivel Local"]=$valor;
            else if($numero=="100")
                $datos["10$i Autor personal"]=$valor;
            else if($numero=="110")
                $datos["11$i Autor corporativo"]=$valor;
            else if($numero=="111")
                $datos["12$i Autor por asamblea, congreso, conferencia, etc."]=$valor;
            else if($numero=="201")
                $datos["13$i Título abreviado"]=$valor;
            else if($numero=="240")
            {
                if($tipo=="A")
                    $datos["14$i Volumen, Número, Páginas"]=$valor;
                else
                    $datos["14$i Título Uniforme"]=$valor;
            }
            else if($numero=="243")
                $datos["15$i Título Uniforme Colectivo"]=$valor;
            else if($numero=="245")
            {
                if($tipo=="R")
                {
                    if(array_key_exists('16  Título',$datos))
                        $datos["16  Título"].=" / $valor";
                    else
                        $datos["16  Título"]=$valor;
                }
                else if($tipo=="A")
                {
                    if(array_key_exists('16  Título del Artículo',$datos))
                        $datos["16  Título del Artículo"].=" / $valor";
                    else
                        $datos["16  Título del Artículo"]=$valor;
                }
                else
                {
                    if(array_key_exists('16  Título/Mención de responsabilidad',$datos))
                        $datos["16  Título/Mención de responsabilidad"].=" / $valor";
                    else
                        $datos["16  Título/Mención de responsabilidad"]=$valor;
                }
            }
            else if($numero=="246")
            {
                if($tipo=="R")
                    $datos["17$i Título paralelo"]=$valor;
                else
                    $datos["17$i Variante del Título"]=$valor;
            }
            else if($numero=="250")
                $datos["18$i Edición / Mención de responsabilidad"]=$valor;
            else if($numero=="260")
            {
                if($tipo=="R")
                {
                    if(array_key_exists('19  Pie de imprenta',$datos))
                        $datos["19  Pie de imprenta"][]=$valor;
                    else
                        $datos["19  Pie de imprenta"]=array($valor);
                }
                else
                {
                    if(array_key_exists('19  Lugar : Editorial',$datos))
                        $datos["19  Lugar : Editorial"][]=$valor;
                    else
                        $datos["19  Lugar : Editorial"]=array($valor);
                }
            }
            else if($numero=="300")
            {
               if($tipo=="R")
                   $datos["20$i Descripción física"]=$valor;
               else
                   $datos["20$i Páginas o volumenes"]=$valor;
            }
            else if($numero=="306")
               $datos["21$i Duración del material"]=$valor;
            else if($numero=="400")
               $datos["22$i Nota de serie bajo autor personal"]=$valor;
            else if($numero=="410")
               $datos["23$i Nota de serie bajo autor corporativo"]=$valor;
            else if($numero=="411")
               $datos["24$i Nota de serie bajo autor por asamblea, congreso, conferencia, etc."]=$valor;
            else if($numero=="440")
               $datos["25$i Serie"]=$valor;
            else if($numero=="490"){
		if(array_key_exists('26  Nota de serie',$datos))
                        $datos["26  Nota de serie"].=" ; $valor";
                    else
                        $datos["26  Nota de serie"]=$valor;
		}

            else if($numero=="500")
            {
		if($tipo=="R"){
					if(array_key_exists('27  Notas',$datos))
                        $datos["27  Notas"].=" / $valor";
                    else
                        $datos["27  Notas"]=$valor;
               }
               else{
				   if(array_key_exists('27  Notas generales',$datos))
                        $datos["27  Notas generales"].=" / $valor";
                    else
                        $datos["27  Notas generales"]=$valor;
               }
            }
            else if($numero=="501")
               $datos["29$i Notas \"Con\""]=$valor;
            else if($numero=="502")
               $datos["30$i Nota de tesis"]=$valor;
            else if($numero=="503")
               $datos["31$i Nota de historial bibliográfico"]=$valor;
            else if($numero=="504")
               $datos["32$i Nota de bibliográfia"]=$valor;
            else if($numero=="505"){
		if(array_key_exists('33  Nota de contenido',$datos))
                        $datos["33  Nota de contenido"].=" -- $valor";
                    else
                        $datos["33  Nota de contenido"]=$valor;
		}
            else if($numero=="506")
               $datos["34$i Nota de restricciones de acceso"]=$valor;
            else if($numero=="508")
               $datos["35$i Notas de Crédito"]=$valor;
            else if($numero=="510")
               $datos["36$i Nota de referencia/cita"]=$valor;
            else if($numero=="511")
               $datos["37$i Notas de elenco"]=$valor;
            else if($numero=="518")
               $datos["38$i Nota sobre fecha/hora y lugar de grabación"]=$valor;
            else if($numero=="520")
            {
               if($tipo=="A")
                   $datos["39$i Resumen"]=$valor;
               else
                   $datos["39$i Nota de resumen"]=$valor;
            }
            else if($numero=="521")
               $datos["40$i Nota de audiencia"]=$valor;
            else if($numero=="530")
               $datos["41$i Nota sobre otros formatos disponibles"]=$valor;
	   else if($numero=="533"){
               //$datos["42 NNotas de reproducción"]=$valor;
		if(array_key_exists("42 NNotas de reproducción",$datos))
						$datos["42 NNotas de reproducción"].= ($posicion==1) ? ", $valor " : " $valor";
					else
						$datos["42 NNotas de reproducción"]= ($posicion==1) ? " $valor " : " $valor";
	   }
            else if($numero=="534")
               $datos["43$i Nota de versión original"]=$valor;
            else if($numero=="538")
               $datos["44$i Nota de detalle de sistema"]=$valor;
            else if($numero=="546")
               $datos["45$i Nota de idioma"]=$valor;
            else if($numero=="583")
               $datos["46$i Acción de conservación"]=$valor;
            else if($numero=="590")
               $datos["47$i Nota de catalogación personalizada"]=$valor;
            else if($numero=="600")
               $datos["48$i Encabezamiento bajo autor personal"]=$valor;
            else if($numero=="610")
               $datos["49$i Encabezamiento bajo autor corporativo"]=$valor;
            else if($numero=="611")
               $datos["50$i Encabezamiento bajo autor por asamblea, congreso, conferencia, etc."]=$valor;
            else if($numero=="630")
               $datos["51$i Encabezamiento bajo título uniforme"]=$valor;
            else if($numero=="650")
            {
		if($tipo=="R"){
					
					if(array_key_exists("52 DDescriptores",$datos))
						$datos["52 DDescriptores"].= ($posicion==1) ? ", $valor " : " - $valor";
					else
						$datos["52 DDescriptores"]= ($posicion==1) ? " $valor " : " - $valor";
										
				}
                else{
					if(array_key_exists("52 EEncabezamiento bajo temas generales",$datos))
						$datos["52 EEncabezamiento bajo temas generales"].= ($posicion==1) ? ", $valor " : " - $valor";
					else
						$datos["52 EEncabezamiento bajo temas generales"]= ($posicion==1) ? " $valor " : " - $valor";
		}
		/*
                if($tipo=="R")
                    $datos["51$i Descriptores"]=$valor;
                else
                    $datos["51$i Encabezamiento bajo temas generales"]=$valor;
		*/
            }
            else if($numero=="651")
                $datos["53$i Encabezamiento bajo nombres geográficos"]=$valor;
            else if($numero=="700")
            {
                if($tipo=="R")
                    $datos["54$i Fecha de inicio - terminación"]=$valor;
                else if($tipo=="A")
                    $datos["54$i Asientos secundarios"]=$valor;
                else
                    $datos["54$i Asientos secundarios bajo autor personal"]=$valor;
            }
            else if($numero=="710")
               $datos["55$i Asientos secundarios bajo autor corporativo"]=$valor;
            else if($numero=="711")
               $datos["56$i Asientos secundarios bajo autor por asamblea, congreso, conferencia, etc."]=$valor;
            else if($numero=="730")
               $datos["57$i Asientos secundarios bajo título uniforme"]=$valor;
            else if($numero=="740")
               $datos["58$i Asientos secundarios de título (diferente de la portada)"]=$valor;
            else if($numero=="760")
               $datos["59$i Fecha de fascículos no recibidos"]=$valor;
            else if($numero=="773")
               $datos["59$i Vol., Num.(Año), Pags."]=$valor;
            else if($numero=="780")
               $datos["60$i Acervo"]=$valor;
            else if($numero=="856")
            {
               if(!preg_match('|^[a-zA-Z0-9]{3}$|i',trim($valor)) && !preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', trim($valor)))
					$descripcion=$valor;
					
				if (preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', trim($valor))) 
					$url=$valor;
                
				
			    if(isset($url) && isset($descripcion))
				{
					$et="";
			   		   
					if($tipo=="A")
						$et="61$i Recursos digitales";
					else
						$et="61$i Líga a los recursos electrónicos";
				   
                    $datos[$et]="<a href='JavaScript:html5Lightbox.showLightbox(0, \"$url\", \"".addslashes(str_replace("'","",$datos["16  Título/Mención de responsabilidad"]))."\");' >$descripcion</a>";
				    unset($descripcion);
					unset($url);
				}
			   
            }
            else if($numero=="999")
               $datos["62$i Detalles:"]=$valor;
            
            $i++;
        }
        
        if(array_key_exists('19  Pie de imprenta',$datos))
        {
            $primer=array_shift($datos["19  Pie de imprenta"]);
            $datos["19  Pie de imprenta"]="$primer ".join($datos["19  Pie de imprenta"],', ');
        }
        else if(array_key_exists('19  Lugar : Editorial',$datos))
        {
            $primer=array_shift($datos["19  Lugar : Editorial"]);
            $datos["19  Lugar : Editorial"]="$primer ".join($datos["19  Lugar : Editorial"],', ');
        }
        
        ksort($datos);
        return $datos;
    }
}
