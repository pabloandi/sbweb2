<?php

class Fichasgeneral extends BaseFichasgeneral
{
    public function getDatosEtiquetasMarc()
    {
	$raw=str_replace('Ì','',$this->getEtiquetasmarc());
        $etiqueta=explode('¦',$raw);
        $datos=array();
        
        $datos['Ficha_No']=$this->getPrimaryKey();
        
        foreach($etiqueta as $actual)
        {
            $numero=substr($actual,0,3);
            $valor=substr($actual,3);
            $tipo=$datos['Ficha_No'][0];
            
            if($numero=="000")
                $datos['Título Revista']=$valor;
            else if($numero=="020")
                $datos['ISBN']=$valor;
            else if($numero=="040")
                $datos['Fuente de catalogación']=$valor;
            else if($numero=="041")
                $datos['Código de lengua']=$valor;
            else if($numero=="043")
                $datos['Código de área geográfica']=$valor;
            else if($numero=="045")
                $datos['Cronológico ó Fecha/Hora']=$valor;
            else if($numero=="050")
            {
                if($tipo=="R")
                    $datos['Asignatura topográfica']=$valor;
                else
                    $datos['Clasificacion LC']=$valor;
            }
            else if($numero=="082")
                $datos['Clasificacion Dewey']=$valor;
            else if($numero=="090")
                $datos['Clasificacion a nivel Local']=$valor;
            else if($numero=="100")
                $datos['Autor personal']=$valor;
            else if($numero=="110")
                $datos['Autor corporativo']=$valor;
            else if($numero=="111")
                $datos['Autor por asamblea, congreso, conferencia, etc.']=$valor;
            else if($numero=="201")
                $datos['Título abreviado']=$valor;
            else if($numero=="240")
            {
                if($tipo=="A")
                    $datos['Volumen, Número, Páginas']=$valor;
                else
                    $datos['Título Uniforme']=$valor;
            }
            else if($numero=="243")
                $datos['Título Uniforme Colectivo']=$valor;
            else if($numero=="245")
            {
                if($tipo=="R")
                    $datos['Título']=$valor;
                else if($tipo=="A")
                    $datos['Título del Artículo']=$valor;
                else
                    $datos['Título/Mención de responsabilidad']=$valor;
            }
            else if($numero=="246")
            {
                if($tipo=="R")
                    $datos['Título paralelo']=$valor;
                else
                    $datos['Variante del Título']=$valor;
            }
            else if($numero=="250")
                $datos['Edición / Mención de responsabilidad']=$valor;
            else if($numero=="260")
            {
                if($tipo=="R")
                    $datos['Pie de imprenta']=$valor;
                else
                    $datos['Lugar / Editorial']=$valor;
            }
            else if($numero=="300")
            {
                if($tipo=="R")
                    $datos['Descripción física']=$valor;
                else
                    $datos['Páginas o volumenes']=$valor;
            }
            else if($numero=="306")
                $datos['Duración del material']=$valor;
            else if($numero=="400")
                $datos['Nota de serie bajo autor personal']=$valor;
            else if($numero=="410")
                $datos['Nota de serie bajo autor corporativo']=$valor;
            else if($numero=="411")
                $datos['Nota de serie bajo autor por asamblea, congreso, conferencia, etc.']=$valor;
            else if($numero=="440")
                $datos['Serie']=$valor;
            else if($numero=="490")
                $datos['Nota de serie']=$valor;
            else if($numero=="500")
            {
                if($tipo=="R")
                    $datos['Notas']=$valor;
                else
                    $datos['Notas generales']=$valor;
            }
            else if($numero=="501")
                $datos['Notas "Con"']=$valor;
            else if($numero=="502")
                $datos['Nota de tesis']=$valor;
            else if($numero=="503")
                $datos['Nota de historial bibliográfico']=$valor;
            else if($numero=="504")
                $datos['Nota de bibliográfia']=$valor;
            else if($numero=="505")
                $datos['Nota de contenido']=$valor;
            else if($numero=="506")
                $datos['Nota de restricciones de acceso']=$valor;
            else if($numero=="508")
                $datos['Notas de Crédito']=$valor;
            else if($numero=="510")
                $datos['Nota de referencia/cita']=$valor;
            else if($numero=="511")
                $datos['Notas de elenco']=$valor;
            else if($numero=="518")
                $datos['Nota sobre fecha/hora y lugar de grabación']=$valor;
            else if($numero=="520")
                
            {
                if($tipo=="A")
                    $datos['Resumen']=$valor;
                else
                    $datos['Nota de resumen']=$valor;
            }
            else if($numero=="521")
                $datos['Nota de audiencia']=$valor;
            else if($numero=="530")
                $datos['Nota sobre otros formatos disponibles']=$valor;
            else if($numero=="534")
                $datos['Nota de versión original']=$valor;
            else if($numero=="538")
                $datos['Nota de detalle de sistema']=$valor;
            else if($numero=="546")
                $datos['Nota de idioma']=$valor;
            else if($numero=="583")
                $datos['Acción de conservación']=$valor;
            else if($numero=="590")
                $datos['Nota de catalogación personalizada']=$valor;
            else if($numero=="600")
                $datos['Encabezamiento bajo autor personal']=$valor;
            else if($numero=="610")
                $datos['Encabezamiento bajo autor corporativo']=$valor;
            else if($numero=="611")
                $datos['Encabezamiento bajo autor por asamblea, congreso, conferencia, etc.']=$valor;
            else if($numero=="630")
                $datos['Encabezamiento bajo título uniforme']=$valor;
            else if($numero=="650")
            {
                if($tipo=="R")
                    $datos['Descriptores']=$valor;
                else
                    $datos['Encabezamiento bajo temas generales']=$valor;
            }
            else if($numero=="651")
                $datos['Encabezamiento bajo nombres geográficos']=$valor;
            else if($numero=="700")
            {
                if($tipo=="R")
                    $datos['Fecha de inicio - terminación']=$valor;
                else if($tipo=="A")
                    $datos['Asientos secundarios']=$valor;
                else
                    $datos['Asientos secundarios bajo autor personal']=$valor;
            }
            else if($numero=="710")
                $datos['Asientos secundarios bajo autor corporativo']=$valor;
            else if($numero=="711")
                $datos['Asientos secundarios bajo autor por asamblea, congreso, conferencia, etc.']=$valor;
            else if($numero=="730")
                $datos['Asientos secundarios bajo título uniforme']=$valor;
            else if($numero=="730")
                $datos['Asientos secundarios de título (diferente de la portada)']=$valor;
            else if($numero=="760")
                $datos['Fecha de fascículos no recibidos']=$valor;
            else if($numero=="780")
                $datos['Acervo']=$valor;
            else if($numero=="856")
                
            {
                if($tipo=="A")
                    $datos['Recursos digitales']=$valor;
                else
                    $datos['Líga a los recursos electrónicos']=$valor;
            }
        }
        
        return $datos;
    }
}
