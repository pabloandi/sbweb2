<?php

class FichasgeneralPeer extends BaseFichasgeneralPeer
{
    public static function verificarPalabraEnVetados($palabra,$vetados)
    {
        foreach($vetados AS $vetado)
        {
            if($palabra==$vetado)
                return true;
        }
        
        return false;
    }
    
    public static function removerPalabrasIrrelevantes($palabras)
    {
        $palabrasVetadas=array('el','los','la','las','de','del','y','o','ó','+','-','*','/');
        
        return array_diff($palabras, $palabrasVetadas); 
    }
    
     public static function prepararConsulta($palabras)
    {
        $c = new Criteria();
//         $c->setDbName('biblioteca_tulua');
//         if(count($palabrasOr)!=0)
        if(count(self::removerPalabrasIrrelevantes(str_word_count($palabras,1,'áéíóúñ')))>0)
        {
            
            $palabrasAnd=$palabrasOr=FichasgeneralPeer::removerPalabrasIrrelevantes(str_word_count($palabras,1,'áéíóúñ'));
            
            $crit1=$c->getNewCriterion(FichasgeneralPeer::TITULO, "%$palabras%", Criteria::LIKE);
            
            $crit2=Null;
            
            
            if(count($palabrasAnd)>1)
            {
               
                $primeraPalabra=array_shift($palabrasAnd);
                
                $crit2=$c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%".$primeraPalabra."%", Criteria::LIKE);
                
                foreach($palabrasAnd AS $p)
                {
    //                 if(!FichasgeneralPeer::verificarPalabraEnVetados($p,$palabrasVetadas))
                        $crit2->addAnd($c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%$p%", Criteria::LIKE));
                    
                }
            }
            else
            {
                $crit2=$c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%".$palabrasAnd."%", Criteria::LIKE);
            }
            
            $crit3=Null;
            
            if(count($palabrasOr)>1)
            {
                $primeraPalabra=array_shift($palabrasOr);
                
                $crit3=$c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%".$primeraPalabra."%", Criteria::LIKE);
                
                foreach($palabrasOr AS $p)
                {
    //                 if(!FichasgeneralPeer::verificarPalabraEnVetados($p,$palabrasVetadas))
                        $crit3->addAnd($c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%$p%", Criteria::LIKE));
                    
                }
            }
            else
            {
                $crit3=$c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%".$palabrasOr."%", Criteria::LIKE);
            }
            
//             $c->addOr($crit1);
//             $c->addAnd($crit2);
//             $c->addAnd($crit3);
               
                $crit1->addOr($crit2);
                $crit1->addOr($crit3);
                $c->add($crit1);
                
        }
        else
        {
            $crit1=$c->getNewCriterion(FichasgeneralPeer::TITULO, "%$palabras%", Criteria::LIKE);
            $crit2=$c->getNewCriterion(FichasgeneralPeer::AUTOR, "%$palabras%", Criteria::LIKE);
            $crit3=$c->getNewCriterion(FichasgeneralPeer::ETIQUETASMARC, "%$palabras%", Criteria::LIKE);
            
            $crit1->addOr($crit2);
            $crit1->addOr($crit3);
            $c->add($crit1);
        }
        
        return $c;
    }
    
    public static function buscar($palabras,$categoria){

      $whereBase='MATCH(Fichasgeneral.Etiquetasmarc,Fichasgeneral.Titulo,Fichasgeneral.Autor,Fichasgeneral.Clasificacion,Fichasgeneral.Isbn, Fichasgeneral.Fechapublicacion, Fichasgeneral.Tipomaterial) AGAINST (_utf8 ?)';
      $whereParams=array();
      $whereParams=array($palabras);

      switch ($categoria) {
          case 'Li':
                
              break;
          case 'titulo':
                $whereParams=array("%$palabras%");
                $whereBase='Fichasgeneral.Titulo LIKE _utf8 ?';
                break;
          case 'autor':
                $whereParams=array("%$palabras%");
                $whereBase='Fichasgeneral.Autor LIKE _utf8 ?';
                break;
          case 'tipomaterial':
                $whereParams=array("%$palabras%");
                $whereBase='Fichasgeneral.Tipomaterial LIKE _utf8 ?';
                break;
          default:
              $whereBase.=" AND Fichasgeneral.FichaNo LIKE ?";
              $whereParams=array($palabras,"$categoria%");
              break;
      }

      $q=DbFinder::from('Fichasgeneral')->whereCustom($whereBase,$whereParams);

      return $q;

    }
    
    
}
