<?php

require_once dirname(__FILE__).'/../lib/buzonGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/buzonGeneratorHelper.class.php';

/**
 * buzon actions.
 *
 * @package    sbweb
 * @subpackage buzon
 * @author     websoft
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class buzonActions extends autoBuzonActions
{
  public function executeResponderSugerencia($request){
      $id = $request->getParameter('id');
      $sugerencia = BuzonPeer::retrieveByPK($id);
      if($sugerencia){

          $sugerencia->setRecibido(true);

          try{
            $sugerencia->save();
          }
          catch(Exception $e){

          }

      }

      $this->redirect("@buzon");
  }
}
