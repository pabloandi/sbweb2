<style>
th, td{
	border: 0px !important;
	
}
</style>

<div id="login-form" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons"  style="position: absolute; height: auto; width: 350px;">
	<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix">
		<span id="ui-id-1" class="ui-dialog-title">Acceder</span>
	</div>
	
	<div id="dialog-form" class="ui-dialog-content ui-widget-content" style="width: auto; min-height: 0px; max-height: none; height: 206px;">
		<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
		  <table>
			    <?php if($form['username']->hasError()): ?>
					<tr>
						<td colspan=2>
							<?php echo $form['username']->renderError() ?>
						</td>
					</tr>
				<?php endif; ?>
				<tr>
					<td><label for="signin_username">Usuario</label></td>
					<td><?php echo $form['username']->render(array('class'=>'text ui-widget-content ui-corner-all')) ?></td>
				</tr>
				<?php if($form['password']->hasError()): ?>
					<tr>
						<td colspan=2>
							<?php echo $form['password']->renderError() ?>
						</td>
					</tr>
				<?php endif; ?>
				<tr>
					<td><label for="signin_password">Contraseña</td>
					<td><?php echo $form['password']->render(array('class'=>'text ui-widget-content ui-corner-all')) ?></td>
				</tr>
				<tr>
					<td><label for="signin_remember">Recordar</label></td>
					<td><?php echo $form['remember']->render(array('class'=>'text ui-widget-content ui-corner-all')) ?></td>
				</tr>
		  </table>
		  <div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix">
			<input type="submit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" value="Acceder" />
		 </div>
		</form>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function() {
	$( "#login-form" ).position({
		my: "center top",
		at: "center bottom",
		of: "body"
	});
});

</script>

