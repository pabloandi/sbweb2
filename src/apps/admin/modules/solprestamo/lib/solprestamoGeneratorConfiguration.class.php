<?php

/**
 * solprestamo module configuration.
 *
 * @package    sbweb
 * @subpackage solprestamo
 * @author     websoft
 * @version    SVN: $Id: configuration.php 12474 2008-10-31 10:41:27Z fabien $
 */
class solprestamoGeneratorConfiguration extends BaseSolprestamoGeneratorConfiguration
{
	public function getFilterDefaults()
	{
		return array('estado_id'=> 1);
	}
}
