<?php

require_once dirname(__FILE__).'/../lib/solprestamoGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/solprestamoGeneratorHelper.class.php';

/**
 * solprestamo actions.
 *
 * @package    sbweb
 * @subpackage solprestamo
 * @author     websoft
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class solprestamoActions extends autoSolprestamoActions
{
    public function executeListAutorizar(sfWebRequest $request){
        $solprestamo=SolicitudesprestamoPeer::retrieveByPK($request->getParameter('id'));
        $this->forward404Unless($solprestamo,'No existe la solicitud de prestamo con el id '.$request->getParameter('id'));
        
        try{
            $solprestamo->setEstadoId(2);
            $solprestamo->save();

            
                        
         }
        catch (PropelException $e){
            $this->getUser()->setFlash('error', sprintf('La solicitud no ha sido guardado debido a errores: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }
        catch (Exception $ex){
            $this->getUser()->setFlash('error', sprintf('Error general: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }

        $this->getUser()->setFlash('notice', "La solicitud ha sido actualizada satisfactoriamente");
        $this->redirect("@solicitudesprestamo");

        
    }

    public function executeListRechazar(sfWebRequest $request){
        $solprestamo=SolicitudesprestamoPeer::retrieveByPK($request->getParameter('id'));
        $this->forward404Unless($solprestamo,'No existe la solicitud de prestamo con el id '.$request->getParameter('id'));

        try{
            $solprestamo->setEstadoId(3);
            $solprestamo->save();


        }
        catch (PropelException $e){
            $this->getUser()->setFlash('error', sprintf('La solicitud no ha sido guardado debido a errores: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }
        catch (Exception $ex){
            $this->getUser()->setFlash('error', sprintf('Error general: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }

        $this->getUser()->setFlash('notice', "La solicitud ha sido actualizada satisfactoriamente");
        $this->redirect("@solicitudesprestamo");
    }

    public function executeListRecibido(sfWebRequest $request){
        $solprestamo=SolicitudesprestamoPeer::retrieveByPK($request->getParameter('id'));
        $this->forward404Unless($solprestamo,'No existe la solicitud de prestamo con el id '.$request->getParameter('id'));

        try{
            $solprestamo->setEstadoId(4);
            $solprestamo->save();


        }
        catch (PropelException $e){
            $this->getUser()->setFlash('error', sprintf('La solicitud no ha sido guardado debido a errores: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }
        catch (Exception $ex){
            $this->getUser()->setFlash('error', sprintf('Error general: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }

        $this->getUser()->setFlash('notice', "La solicitud ha sido actualizada satisfactoriamente");
        $this->redirect("@solicitudesprestamo");
    }

    public function executeListNoRecibido(sfWebRequest $request){
        $solprestamo=SolicitudesprestamoPeer::retrieveByPK($request->getParameter('id'));
        $this->forward404Unless($solprestamo,'No existe la solicitud de prestamo con el id '.$request->getParameter('id'));

        try{
            $solprestamo->setEstadoId(5);
            $solprestamo->save();


        }
        catch (PropelException $e){
            $this->getUser()->setFlash('error', sprintf('La solicitud no ha sido guardado debido a errores: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }
        catch (Exception $ex){
            $this->getUser()->setFlash('error', sprintf('Error general: %s',$ex->getMessage()));
            $this->redirect("@solicitudesprestamo");
        }

        $this->getUser()->setFlash('notice', "La solicitud ha sido actualizada satisfactoriamente");
        $this->redirect("@solicitudesprestamo");
    }

    
}
