<?php

/**
 * opac actions.
 *
 * @package    sf_sandbox
 * @subpackage opac
 * @author     websoft
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class opacActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function executeIndex(sfWebRequest $request)
  {

    $this->bibliotecas=DbFinder::from("Bibliotecas")->where('activo',1)->find();
  }

  /**
   * Muestra los ejemplares de una revista
   * @param sfWebRequest $request
   */
  public function executeMostrarEjemplares(sfWebRequest $request)
  {
    //$biblioteca=$request->getParameter('biblioteca_actual_busqueda',1);

    if($request->hasParameter("fichas"))
        $ids=$request->getParameter("fichas");
    elseif($this->getUser()->hasAttribute('pager_ejemplares'))
        $ids=$this->getUser()->getAttribute('pager_ejemplares');

    $this->getUser()->setAttribute('pager_ejemplares',$ids);

    $page=$request->getParameter("page",1);


    $this->pager=$this->consultarFichasPorId($ids)->paginate($page, 1);

    $this->pager->setPage($page);
    $this->pager->init();



    $this->ficha_actual=$this->pager->getObjectByCursor($page);
    //die($this->ficha_actual->getPrimaryKey());

    /** Sección especial para el museo de la Tertulia **/



    if($this->getUser()->hasAttribute('version_siabuc') && $this->getUser()->getAttribute('version_siabuc')=='8'){
        $this->datos_ficha=$this->ficha_actual->getDatosEtiquetasMarc();
        $this->version_siabuc=8;
    }
    else{
        $this->datos_ficha=EtiquetasmarcPeer::getEtiquetasMarc($this->ficha_actual,$this->ficha_actual->getBiblioteca());
        $this->version_siabuc=9;
    }


    $this->ejemplares=$this->consultarEjemplaresPorFicha($this->ficha_actual->getFichaNo(),$this->ficha_actual->getBiblioteca())->find();
    $this->biblioteca=$this->ficha_actual->getBiblioteca();
    $this->tipo=substr($this->ficha_actual->getFichaNo(),0,1);

  }

  private function consultarEjemplaresPorFicha($ficha,$biblioteca){
      //$con=Propel::getConnection($biblioteca);
      //return DbFinder::from('Ejemplaresgeneral',$con)->where('FichaNo', $ficha);
      return DbFinder::from('Ejemplaresgeneral')->where('FichaNo', $ficha)->where('Biblioteca',$biblioteca);
      //return DbFinder::from('Ejemplaresgeneral')->where('FichaNo', $ficha);

  }

  /**
   * Consulta de fichas
   * @param string $texto
   * @param string $categoria
   * @param PropelConnection $con
   * @return DBfinder object
   */
  private function consultarFichas($texto, $categoria, $biblioteca){

      $finder=FichasgeneralPeer::buscar($texto, $categoria);


        if($biblioteca=="all")
			return $finder;
		else
			return $finder->where('Biblioteca',$biblioteca);
  }

  /**
   * Consulta fichas por identificacion primaria
   * @param <type> $ids
   * @param <type> $biblioteca
   */
  private function consultarFichasPorId($ids){
      //$con=Propel::getConnection($biblioteca);
		$finder=DbFinder::from('Fichasgeneral');
		if(count($ids)>1) {
			$finder=$finder->whereCustom('Fichasgeneral.FichaNo = ? AND Fichasgeneral.Biblioteca = ?',explode("-",$ids[0]));
			foreach(array_slice($ids,1) as $ficha){
				$finder=$finder->orWhereCustom('Fichasgeneral.FichaNo = ? AND Fichasgeneral.Biblioteca = ?',explode("-",$ficha));
			}
		}
		else
			$finder=$finder->whereCustom('Fichasgeneral.FichaNo = ? AND Fichasgeneral.Biblioteca = ?',explode("-",$ids[0]));


	  return $finder;
  }

  public function executeConsultar(sfWebRequest $request)
  {

    $biblioteca=$request->getParameter('biblioteca_actual_busqueda','all');

    $page=$request->getParameter('page');
    $limit=$request->getParameter('rows',10);
    $sidx=$request->getParameter('sidx');
    $sord=$request->getParameter('sord');
    $txtLIB=$request->getParameter('txtLIB');

    $categoria=$request->getParameter('categoria','L');

    $this->pager=$this->consultarFichas($txtLIB, $categoria, $biblioteca)->paginate($page,$limit);

    $this->txtLIB=$txtLIB;
    $this->biblioteca=$biblioteca;
    $this->categoria=$categoria;

  }



  public function executeConsultarPorBiblioteca(sfWebRequest $request)
  {

    //$bds=sfYaml::load(sfConfig::get('sf_config_dir').DIRECTORY_SEPARATOR.'conexiones.yml');
    $bds=DbFinder::from("Bibliotecas")->find();

    $txtLIB=$request->getParameter('txtLIB');
    $txtTIT=$request->getParameter('txtTIT');
    $txtAut=$request->getParameter('txtAut');
    $txtTEM=$request->getParameter('txtTEM');
    $categoria=$request->getParameter('categoria','L');

    $bibliotecas=array();

    foreach($bds AS $biblioteca)
    {
        $finder=$this->consultarFichas($txtLIB, $categoria, $biblioteca->getPrimaryKey());
        $bibliotecas[$biblioteca->getPrimaryKey()]['resultados']=$finder->count();
	$bibliotecas[$biblioteca->getPrimaryKey()]['nombre']=$biblioteca->getNombre();
	$bibliotecas[$biblioteca->getPrimaryKey()]['version_siabuc']=$biblioteca->getSiabucversion();


    }

    $this->txtLIB=$txtLIB;
    $this->bibliotecas=$bibliotecas;

  }



  public function executeVerDetallesUsuario(sfWebRequest $request)
  {

    $codigo=$request->getParameter("codigo_usuario");
    $this->usuario = DbFinder::from('Usuariosgeneral')->findPk($codigo);
    //die($this->usuario);
    $this->forward404Unless($this->usuario);

    $this->totalMultas=0;

    $multas = DbFinder::from('Multaspendientes')->where('Nocuenta', $codigo)->find();
    $prestamos = DbFinder::from('Prestamosgeneral')->where('Nocuenta',$codigo)->find();

    if(count($multas)>0){
        foreach ($multas as $multa){
            $this->totalMultas += (int) $multa->getMonto();
        }
    }

    $this->multas=$multas;
    $this->prestamos=$prestamos;
    $this->solicitudes=DbFinder::from('Solicitudesprestamo s')->
          where("nocuenta",$codigo)->
          whereCustom('DATEDIFF(now(),s.UpdatedAt) < ?',8,'cond6')->
          find();

  }

  public function executeReservarEjemplares(sfWebRequest $request)
  {
    $this->enviarCorreoBibliotecario($request);
    $this->getUser()->getAttributeHolder()->clear();
    return $this->renderText('Se ha enviado el correo con la solicitud de reserva de material');

  }



  protected function enviarCorreoBibliotecario(sfWebRequest $request)
  {
    $codigo=$request->getParameter('codigo_usuario_reserva');
    $nombre=$request->getParameter('nombre_usuario_reserva');
    $correo=$request->getParameter('correo_usuario_reserva');

    $usuario=DbFinder::from('Usuariosgeneral')->findPk($codigo);


	$bibliotecas=array();
	$ejemplares=$request->getParameter('ejemplares_a_reservar');

	foreach($ejemplares as $ejemplar){

		list($ficha_no,$numero_adqui,$biblioteca)=explode("_",$ejemplar);
        $ficha_ejemplar_reservado=DbFinder::from('Fichasgeneral')->findPk(array($ficha_no,$biblioteca));

		try{
            $solp=new Solicitudesprestamo();
            $solp->setNocuenta($codigo);
            $solp->setNumadqui($numero_adqui);
			$solp->setUsuario($nombre);
            $solp->setTitulo($ficha_ejemplar_reservado->getTitulo());
            $solp->setAutor($ficha_ejemplar_reservado->getAutor());
            $solp->setClasificacion($ficha_ejemplar_reservado->getClasificacion());
            $solp->save();
        }
        catch (PropelException $e){
            return $this->renderText("Hubo un error al registrar la solicitud de prestamo: $numero_adqui");
        }

	}

	// return $this->renderText("Su solicitud de reserva ha sido registrada con exito");



  }

  public function executeAgregarEjemplares(sfWebRequest $request)
  {

    $ids=$request->getParameter("ejemplares_seleccionados");

    $ejemplares = $this->getUser()->getAttribute('ejemplares_reservados');

    if(is_array($ids)){
		foreach($ids AS $id)
		{
			$ejemplares[]=$id;
		}
    }
    else{
		$ejemplares[]=$ids;
	}



    $this->getUser()->setAttribute('ejemplares_reservados', array_unique($ejemplares));

    return sfView::NONE;

  }

  public function executeVerEjemplaresReservados(sfWebRequest $request)
  {
    $ejemplares_reservados = array();
    $fichas_ejemplares_reservados = array();

    if($this->getUser()->hasAttribute('ejemplares_reservados'))
    {
        foreach($this->getUser()->getAttribute('ejemplares_reservados') AS $key=>$ejemplar)
        {
            list($ficha_no,$numero_adqui,$biblioteca)=explode("_",$ejemplar);
            $ejemplares_reservados[$key]=DbFinder::from('Ejemplaresgeneral')->findPk(array($ficha_no,$numero_adqui,$biblioteca));
            $fichas_ejemplares_reservados[$key]=DbFinder::from('Fichasgeneral')->findPk(array($ficha_no,$biblioteca));
        }
    }

    $this->forward404If(count($ejemplares_reservados)==0);

    $this->ejemplares=$ejemplares_reservados;
    $this->fichas=$fichas_ejemplares_reservados;
    $this->biblioteca=$biblioteca;
  }

  public function executeRechazarEjemplar(sfWebRequest $request)
  {

    $ejemplar_rechazado=$request->getParameter('ejemplar_rechazado');
    $ejemplares=$this->getUser()->getAttribute('ejemplares_reservados');
    unset($ejemplares[$ejemplar_rechazado]);

    $this->getUser()->setAttribute('ejemplares_reservados',$ejemplares);

    $ids = array();
    $fichas_ejemplares_reservados=array();

    foreach($this->getUser()->getAttribute('ejemplares_reservados') AS $key=>$ejemplar)
    {

        list($ficha_no,$numero_adqui,$biblioteca)=explode("_",$ejemplar);
        $ids[$key]=DbFinder::from('Ejemplaresgeneral')->findPk(array($ficha_no,$numero_adqui,$biblioteca));
        $fichas_ejemplares_reservados[$key]=DbFinder::from('Fichasgeneral')->findPk(array($ficha_no,$biblioteca));

    }

//     $this->ejemplares=$ids;

    return $this->renderPartial('opac/ejemplares_reservados',array('ejemplares'=>$ids,'fichas'=>$fichas_ejemplares_reservados));

  }

  public function executeVerificarExistenciaUsuario(sfWebRequest $request)
  {

    $usuario = DbFinder::from('Usuariosgeneral')->findPk($request->getParameter('codigo_usuario'));
    $this->forward404Unless($usuario);

    //return $this->renderText($usuario->getNombre());
    return $this->renderPartial('opac/datos_usuario',array('usuario'=>$usuario));

  }

  public function executeContactarInstitucion(sfWebRequest $request)
  {
    $this->bibliotecas=DbFinder::from("Bibliotecas")->find();
  }

  public function executeSeleccionarBiblioteca(sfWebRequest $request)
  {
    $bibliotecas = DbFinder::from('Bibliotecas')->find();

    $this->bibliotecas=$bibliotecas;
  }

  public function executeBibliotecaEscogida(sfWebRequest $request){
        $this->getUser()->setAttribute('version_siabuc',$request->getParameter('version_siabuc'));

        return sfView::NONE;
  }



  public function executeEnviarCorreoInstitucion($request)
  {
    $mensaje=$request->getParameter('mensaje_usuario_contacto');
    $nombre=$request->getParameter('nombre_usuario_contacto');
    $correo=$request->getParameter('correo_usuario_contacto');
    $asunto=$request->getParameter('plan_usuario_contacto');

    $biblioteca=DbFinder::from('Bibliotecas')->findPk($request->getParameter('biblioteca_contacto'));


    $remitente=$biblioteca->getCorreo();

    try{
			$sugerencia=new Buzon();
            $sugerencia->setNombre($nombre);
            $sugerencia->setCorreo($correo);
            $sugerencia->setMensaje($mensaje);
            $sugerencia->setAsunto($asunto);
            $sugerencia->setBiblioteca($biblioteca->getNombre());
            $sugerencia->save();
	}
	catch (PropelException $e){
		return $this->renderText("Hubo un error al registrar la sugerencia: $nombre - $asunto");
	}

    return $this->renderText("Su mensaje ha sido enviado exitosamente");
  }



  public function mostrarBibliotecas(sfWebRequest $request)
  {

  }

  public function executeHelp()
  {

  }

  protected function enviarCorreo($remitente, $asunto, $mensaje)
  {
    include_once(sfConfig::get('php_mailer'));//incluimos la libreria phpmailer

    $mail = new PHPMailer();

    //$mail->isMail();
    $mail->isSMTP();
    //$mail->SMTPDebug=2;

    $mail->IsHTML(true); // send as HTML

    $mail->CharSet = 'UTF-8';

    //datos conexión
    $mail->Host = sfConfig::get('app_mail_host');
	$mail->Port = sfConfig::get('app_mail_port');
	$mail->SMTPSecure = sfConfig::get('app_mail_secure');
	$mail->SMTPAuth = true;
	$mail->Username = sfConfig::get('app_mail_username');
	$mail->Password = sfConfig::get('app_mail_secure');



    $mail->Body=$mensaje;

    // Introducimos la información del remitente del mensaje
    $mail->From       = $remitente;
    $mail->FromName   = "Catálogo Web Institución Municipal de Cultura y Fomento al Turismo - Biblioteca Municipal 'RCM'";
    //Asunto
    $mail->Subject    = $asunto;

    $mail->AddAddress($remitente);

    $mail->Send();



  }

  public function executeTestCorreo(sfWebRequest $request)
  {
    $this->enviarCorreo('pabloandi@gmail.com','test','test');
	return $this->renderText("Enviado");
  }



}
