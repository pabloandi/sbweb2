<?php $i=0; ?>
  <?php foreach($bibliotecas as $biblioteca => $datos ): ?>
      <?php echo jq_link_to_remote($datos["nombre"],array(
      "update"    =>  "resultado_consulta",
      "url"       =>  "@consultar",
      "condition" =>  "$('#txtLIB').val()!=''",
      "loading"   =>  "$('#loader-$i').show()",
      "before"    =>  "$('#biblioteca_actual_busqueda').val('$biblioteca');  $('#biblioteca_actual_usuario').val('$biblioteca');",
      "complete"  =>  "
      	$('#loader-$i').hide(); 
      	$('#nombre_biblioteca').html('usted esta en <br/> ".$datos["nombre"]."');
      	$('#nombre_biblioteca').show(); 
      	cargarTabla(); 
      	$.ajax({url: '".url_for('@seleccionar_biblioteca')."',data: {version_siabuc:'".$datos["version_siabuc"]."'}});
      	$('#consulta_bibliotecas').dialog('close');
      	",
      "method"    =>  "get",
      "submit"    =>  "form_busqueda_principal"
      ))
      ?>
	
      <?php echo $datos['resultados'] ?> Coincidencias
<span id="loader-<?php echo $i ?>" style="display:none;" align="center">
<?php echo image_tag('ajax-loader.gif','align=absmiddle') ?>
</span>	
      <br>
	<?php $i++; ?>
  <?php endforeach; ?>
