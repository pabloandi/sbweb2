<?php use_helper('PagerNavigation') ?>


<div id="ficha" class="table-ejemplares" align="center">

  <p>
    <div class="paginador">
        <?php echo pager_navigation($pager,array('url'=>"opac/mostrarEjemplares",'update'=>'ficha','loading'=>'$("#pager1-loader").show()','complete'=>'$("#pager1-loader").hide()')) ?>
        <span id="pager1-loader" style="display:none;"><?php echo image_tag('ajax-loader.gif') ?></span>
    </div>
    
    <br>
    <?php include_partial('ficha',array('datos_ficha'=>$datos_ficha, 'biblioteca'=>$biblioteca, 'version_siabuc'=>$version_siabuc)) ?>
  </p>
  <p>
    <?php if(isset($ejemplares) && count($ejemplares)>0): ?>
        <?php include_partial('ejemplares',array('ejemplares'=>$ejemplares, 'tipo'=>$tipo, 'ficha'=>$ficha_actual)) ?>
      <?php else: ?>
        
      <?php endif ?>
    </p>
  <p>
  <div class="paginador">
    <?php echo pager_navigation($pager,array('url'=>"opac/mostrarEjemplares",'update'=>'ficha','loading'=>'$("#pager2-loader").show()','complete'=>'$("#pager2-loader").hide()')) ?>
    <span id="pager2-loader" style="display:none;"><?php echo image_tag('ajax-loader.gif') ?></span>
  </div>
    
    
      </p>
</div>
