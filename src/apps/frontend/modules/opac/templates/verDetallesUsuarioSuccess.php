<?php setlocale(LC_MONETARY, 'es_CO'); ?>

<table class="font-table" width="700">
    <tr>
        <th width="20%" >Nombre usuario:</th>
		<td width="70%" ><?php echo $usuario->getNombre() ?></td>
    </tr>
    <tr>
      <th >C&oacute;digo</th>
      <td ><?php echo $usuario->getNoCuenta() ?></td>
    </tr>
    <tr>
      <th >Plan de Estudio</th>
      <td ><?php echo $usuario->getNoEscuela() ?></td>
    </tr>
    <tr>
      <th >Grupo:</th>
      <td ><?php echo $usuario->getNoGrupo() ?></td>
    </tr>
    <tr>
      <th >Vigencia:</th>
      <td ><?php echo $usuario->getVigencia() ?></td>
    </tr>
    <?php if ($totalMultas > 0): ?>
	<tr>
      <th >Total Multas:</th>
      <td >$ <?php echo number_format($totalMultas, 0, '.', '') ?> pesos. </td>
    </tr>
	<?php endif; ?>
 </table>
<br />


<br/>
<br/>


    <?php if(isset($prestamos) and count($prestamos)>0): ?>
        <table width="700" border="1" cellspacing="0">
        <tr>
            <th colspan="6" align="center">Prestamos</th>
        </tr>
        <tr class="ui-dialog-titlebar ui-widget-header ui-corner-all">
            <th width="30%" align="center">Titulo</th>
            <th width="30%" align="center">Autor</th>
            <th width="10%" align="center">Fecha Salida</th>
			<th width="10%" align="center">Fecha Entrega</th>
			<th width="10%" align="center">Tipo de prestamo</th>
			<th width="10%" align="center">Biblioteca</th>
        </tr>
        <?php foreach($prestamos AS $prestamo): ?>
        <tr>
            <td width="30%"><?php echo $prestamo->getTitulo() ?></td>
            <td width="30%"><?php echo $prestamo->getAutor() ?></td>
            <td width="10%"><?php echo $prestamo->getFechaSalida() ?></td>
			<td width="10%"><?php echo $prestamo->getFechaEntrega() ?></td>
			<td width="10%"><?php echo $prestamo->getTipoPrestamo() ?></td>
			<td width="10%"><?php echo $prestamo->getBibliotecas() ?></td>
        </tr>
        <?php endforeach; ?>
        </table>

    <?php endif; ?>

  <br/>
  <br/>

  <?php if(isset($multas) and count($multas)>0): ?>
        <table width="700" border="1" cellspacing="0">
        <tr>
            <th colspan="6" align="center">Multas</th>
        </tr>
        <tr class="ui-dialog-titlebar ui-widget-header ui-corner-all">
            <th width="30%" align="center">Fecha devolución</th>
            <th width="30%" align="center">Monto</th>
            <th width="40%" align="center">Observaciones</th>
	</tr>
        <?php foreach($multas AS $multa): ?>
        <tr>
            <td width="30%"><?php echo $multa->getFechaDevolucion() ?></td>
            <td width="30%">$ <?php echo money_format('%i',$multa->getMonto()) ?></td>
            <td width="40%"><?php echo $multa->getObservaciones() ?></td>

        </tr>
        <?php endforeach; ?>

        </table>
    <?php endif; ?>

   <?php if(isset($solicitudes) and count($solicitudes)>0): ?>
        <table width="700" border="1" cellspacing="0" class="font-table">
        <tr class="ui-dialog-titlebar ui-widget-header ui-corner-all">
            <th colspan="6" align="center">Solicitudes de préstamo</th>
        </tr>
        <tr>
            <th class="thead" width="30%" align="center">Titulo</th>
            <th class="thead" width="30%" align="center">Autor</th>
            <th class="thead" width="15%" align="center">Clasificación</th>
            <th class="thead" width="15%" align="center">Estado</th>

        </tr>
        <?php foreach($solicitudes AS $solicitud): ?>
        <tr>
            <td width="30%"><?php echo $solicitud->getTitulo() ?></td>
            <td width="30%"><?php echo $solicitud->getAutor() ?></td>
            <td width="20%"><?php echo $solicitud->getClasificacion() ?></td>
            <td width="20%"><?php echo $solicitud->getEstado() ?></td>

        </tr>
        <?php endforeach; ?>
        </table>
    <?php endif; ?>

  <br/>
  <br/>
