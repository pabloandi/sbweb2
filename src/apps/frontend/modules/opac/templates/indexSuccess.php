<?php use_helper("Form","Tag") ?>

<table  border="0" >

  <tr>
    <td>
        <span class="fg-toolbar ui-widget-header ui-corner-all ui-helper-clearfix">
            <span class="fg-buttonset ui-helper-clearfix">
                <?php echo jq_link_to_remote(content_tag('span','',array('class'=>'ui-icon ui-icon-mail-closed'))."&nbsp;",
                            array(

                                "url"       =>  "opac/contactarInstitucion",
                                "update"    =>  "contacto_institucion",
                                "script"    =>  'true',
                                'loading'   =>  '$("#mostrar-loader").show()',
                                "success"  =>  "$('#mostrar-loader').hide();
												$('<div id=\'contacto_institucion\'></div>').html(data).dialog({
													bgiframe: true,
													autoOpen: true,
													height: 600,
													width: 800,
													modal: true,
													title: 'Contacto',
													close: function(){ $(this).remove(); }
												});
								",
                                "method"    =>  "get",

                                ),
                                array ("class"=>"fg-button ui-state-default fg-button-icon-solo ui-corner-all","title"=>"Contacto"))
                ?>

                <?php echo jq_link_to_remote(content_tag('span','',array('class'=>'ui-icon ui-icon-help'))."&nbsp;",
                            array(
                                "url"       =>  "opac/help",
                                "update"    =>  "ayuda",
                                "script"    =>  'true',
                                "success"  =>  "$('#mostrar-loader').hide();
												$('<div id=\'ayuda\'></div>').html(data).dialog({
													bgiframe: true,
													autoOpen: true,
													height: 580,
													width: 1050,
													modal: true,
													title: 'Ayuda interactiva',
													close: function(){ $(this).remove(); }
												});
								",
                                "method"    =>  "get",
                                ),
                                array ("class"=>"fg-button ui-state-default fg-button-icon-solo ui-corner-all","title"=>"Ayuda"))
                ?>

            </span>
             <span style="float:right;">
                <?php echo jq_form_remote_tag(array(
                        'update'    =>  array('success'=>'consulta_perfil','failure'=>"$('#perfil-loader').hide(); alert('no existe el usuario');"),
                        'url'       =>  'opac/verDetallesUsuario',
                        'condition' =>  '$("#codigo_usuario").val()!=""',
                        'before'    =>  '$("#consulta_perfil").html("")',
                        'loading'   =>  '$("#perfil-loader").show()',

                        "success"  =>  "$('#perfil-loader').hide();
												$('<div id=\'consulta_perfil\'></div>').html(data).dialog({
													bgiframe: true,
													autoOpen: true,
													height: 580,
													width: 1050,
													modal: true,
													title: 'Consulta perfil de usuario',
													close: function(){ $(this).remove(); }
												});
								",
                        "method"    =>  "get",
                        ))
                    ?>
                    <?php echo input_tag('codigo_usuario', '', 'type=password class=input') ?>
                    <?php echo submit_tag('Mi perfil', 'class="fg-button ui-state-default ui-corner-all"') ?> <span id="perfil-loader" style="display:none;"> <?php echo image_tag('ajax-loader.gif','align=absmiddle') ?> </span>

                    </form>
            </span>
	    <div align='right' id='nombre_biblioteca'>

            </div>
        </span>
    </td>
  </tr>

  <tr>
    <td height="446" class="ui-widget-content" >

	<!-- Aqui comienza Busqueda -->
      <table width="100%" border="0" align="center">

        <tr>

          <td align="center">
                <?php echo jq_form_remote_tag(array(
                    'update'    =>  'resultado_consulta',
                    'url'       =>  '@consultar',
                    'loading'   =>  '$("#buscar-loader").show()',
                    'complete'  =>  '$("#buscar-loader").hide(); cargarTabla();',
                    "method"    =>  "get",

                    ),
                    array(
                    "id"        => "form_busqueda_principal",
                    ))
                ?>
              <label>
                <?php echo input_tag('txtLIB', '', array("size"=>'40','maxlength'=>'50','class'=>'input')) ?>
              </label>
              <label>
				  <select id="biblioteca_actual_busqueda" name="biblioteca_actual_busqueda">
					<option selected="selected" value="all">Todas las bibliotecas</option>
					<?php if(count($bibliotecas)>0): foreach($bibliotecas as $biblioteca): ?>
					<option value="<?php echo $biblioteca->getIdbiblioteca() ?>"> <?php echo $biblioteca->getNombre()  ?> </option>
					<?php endforeach; ?>
					<?php endif; ?>
				</select>
              </label>
              <select id="categoria" name="categoria">
                <option selected="selected" value="Li">Busqueda libre</option>
				<option value="titulo">Titulo</option>
				<option value="autor">Autor</option>
				<!--<option value="tipomaterial">Tipo de material</option>-->
                <option value="L">Libros</option>
                <option value="R">Revista</option>
                <option value="A">Artículo</option>
              </select>
              <label>
                <?php echo submit_tag('Buscar','class="fg-button ui-state-default ui-corner-all"') ?>
                <span id="buscar-loader" style="display:none;">
                    <?php echo image_tag('ajax-loader.gif','align=absmiddle') ?>
                </span>
                &nbsp;
              </label>
              <?php //echo input_hidden_tag('biblioteca_actual_busqueda',1) ?>
            </form>
          </td>


        </tr>

        <tr>
          <td align="center">
              <label> </label>
              <div align="center">
                <span id="mostrar-loader" style="display:none;">
                    <?php echo image_tag('ajax-loader.gif','align=absmiddle') ?>
                </span>
                &nbsp;

                <?php echo jq_button_to_remote('Mostrar',
                            array(

								"url"       =>  "opac/mostrarEjemplares",
                                "update"    =>  "consulta_fichas",
                                "condition" =>  "contarCheckboxes()>0",
                                "script"    =>  'true',
                                'loading'   =>  '$("#mostrar-loader").show()',
                                "success"  =>  "$('#mostrar-loader').hide();
												$('<div id=\'consulta_fichas\'></div>').html(data).dialog({
													bgiframe: true,
													autoOpen: true,
													height: 600,
													width: 600,
													modal: true,
													title: 'Consulta Ficha y Ejemplares',
													close: function(){ $(this).remove(); }
												});
								",
                                "method"    =>  "get",
                                //"with"      =>  "'biblioteca_actual_busqueda='+$('#biblioteca_actual_busqueda').val()+'&'+$('#form_tabla_consulta').serialize()"
                                "with"      =>  "seleccionarFichas()"
                                ),
                                array ("class"=>"fg-button ui-state-default ui-corner-all"))
                ?>
                <?php echo jq_button_to_function("Limpiar","$('#txtLIB').val(''); location.reload();",
                                                array ("class"=>"fg-button ui-state-default ui-corner-all"))
                ?>
                <?php echo jq_button_to_remote('Reservar',
				                array(
                                "url"       =>  "opac/verEjemplaresReservados",
								"update"    =>  array(
                                                    'success'   =>  "reserva_ejemplares",
                                                    'failure'   =>  "$('#mostrar-loader').hide(); alert('no existen ejemplares solicitados');"
                                                    ),
                                "script"    =>  'true',
                                "loading"   =>  "$('#mostrar-loader').show()",
                                "success"  =>  "$('#mostrar-loader').hide();
												window.reservar_ejemplares = $('<div id=\'reservar_ejemplares\'></div>').html(data).dialog({
													bgiframe: true,
													autoOpen: true,
													height: 600,
													width: 700,
													modal: true,
													title: 'Solicitud de reserva de ejemplares',
													close: function(){ $(this).remove(); }
												});
								",
                                "method"    =>  "get"

                                ),
                                array ("class"=>"fg-button ui-state-default ui-corner-all"))

                ?>

                <div id="presentacion"></div>
                <div id="pager9" class="scroll" style="text-align:center;"></div>
                <br>
                <div id="resultado_consulta"  ></div>

                <br>
                <div id="loader" style="display:none;" align="center">
                    <?php echo image_tag('ajax-loader.gif') ?>
                </div>

                <!--</form>-->
          </td>

        </tr>
    </table>
        <!-- Aqui Finaliza Busqueda -->

	</td>
  </tr>
  <tr>
    <td class="ui-dialog-titlebar ui-widget-header ui-corner-all font-pie" height="45" align="center">
		&copy; 2014 by
		<a href="<?php echo sfConfig::get('app_urlinstituto') ?>" target="_blank">
			<?php echo sfConfig::get('app_nombreinstituto') ?>
		</a> Todos los derechos reservados.
Impulsado por <a href="http://www.sib.com.co" target="_blank">S.I.B</a></td>
  </tr>
</table>
<div style='margin-top: 20px;'>
    <span class='font-pie'>
        Powered by:
        <br/>
        <br/>
        <a href="http://siabuc.ucol.mx" target='_blank'>
      <img src='http://sib.com.co/sbweb/imagenes/logo_inferior.gif'/>
        </a>
        <br/>
      &copy; Derechos reservados 2006-2015
    </span>
</div>
