<?php use_helper('Form') ?>
    <form id="form-contacto_institucion">
    <table align="center">
            <tr>
                <td>Nombre del usuario</td>
                <td><?php echo input_tag('nombre_usuario_contacto') ?></td>
            </tr>
            <tr>
                <td>Correo del usuario</td>
                <td><?php echo input_tag('correo_usuario_contacto') ?></td>
            </tr>
            <tr>
                <td>Asunto</td>
                <td><?php echo input_tag('plan_usuario_contacto') ?></td>
            </tr>
            <tr>
                <td>Biblioteca</td>
                <td>
					<select name="biblioteca_contacto">
					<?php foreach($bibliotecas as $biblioteca): ?>
					
						<option value="<?php echo $biblioteca->getIdbiblioteca() ?>"> <?php echo $biblioteca->getNombre() ?> </option>
					<?php endforeach; ?>
					</select>
				</td>
            </tr>
            <tr>
                <td colspan="2">Mensaje</td>
            </tr>
            <tr>
                <td colspan="2"><?php echo textarea_tag('mensaje_usuario_contacto',null,'cols=40 rows=10') ?></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    
                    <span id="contacto-inst-loader" style="display:none;">
                        <?php echo image_tag('admin/loading.gif','align=absmiddle') ?>
                    </span>
                </td>
            </tr>
    </table>
    
    </form>
    <div align="center">
    
    <?php echo jq_button_to_remote('Enviar',array(
        'update'    =>  'respuesta_contacto',
        'url'       =>  'opac/enviarCorreoInstitucion',
        'condition' =>  '$("#nombre_usuario_contacto").val()!="" && $("#correo_usuario_contacto").val()!="" && $("#mensaje_usuario_contacto").val()!=""',
        'loading'   =>  '$("#contacto-inst-loader").show()',
//         'success'   =>  'alert("su mensaje ha sido enviado correctamente"); $("#contacto_institucion").dialog("close")',
        'success'   =>  'alert(data); $("#contacto_institucion").dialog("close")',
        'complete'  =>  '$("#contacto-inst-loader").hide()',
        "method"    =>  "post",
        "with"      =>  "$('#form-contacto_institucion').serialize()"
        )
    )
    ?>
	</div>
