
<table width="550px" border="1" cellpadding="0" cellspacing="0" class="table-ficha">
<?php foreach($datos_ficha AS $key =>$valor): ?>
    <?php if($valor!="" && $key!="Ficha_No"): ?>
    <tr>
        <th class="th-table-ficha">
            <?php if($version_siabuc==8): ?>
                <?php echo $key ?>
            <?php else: ?>
                <?php echo trim(substr($key,4)) ?>
            <?php endif; ?>
        </th>
<td class="td-table-ficha">
<?php if(trim(substr($key,4)) == "Líga a los recursos electrónicos"): ?>
		<?php echo $valor ?>
<?php else: ?>
                <?php echo wordwrap($valor,50,'<br/>',true) ?>
<?php endif; ?>
</td>
    </tr>
    <?php endif; ?>
<?php endforeach; ?>
</table>
