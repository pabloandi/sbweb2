<?php use_helper('Form') ?>


    <?php echo jq_form_remote_tag(array(
        'update'    =>  'respuesta_reserva',
        'url'       =>  'opac/reservarEjemplares',
        'condition' =>  '$("#codigo_usuario_reserva").val()!="" && $("#usuario_existente").val()=="1"',
        'loading'   =>  '$("#reserva-ejemplares-loader").show()',
        'success'   =>  'alert(data); window.reservar_ejemplares.dialog("close");',
        'complete'  =>  '$("#reserva-ejemplares-loader").hide()',
        "method"    =>  "post"

    ))
    ?>
    <table>
            <tr>
                <td>Código del usuario</td>
                <td>
                    <?php echo input_tag('codigo_usuario_reserva') ?>
                    <span id="validar-loader" style="display:none;">
                        <?php echo image_tag('ajax-loader.gif','align=absmiddle') ?>
                    </span>

                </td>
                <td>
<?php echo jq_button_to_remote('Validar',
        array(
            "update"    =>  array('success'=>'datos_usuario_reserva','failure'=>"$('#validar-loader').hide(); alert('no existe el usuario');"),
            "url"       =>  "opac/verificarExistenciaUsuario",
            "script"	=>	"true",
            "condition" =>  "$('#codigo_usuario_reserva').val()!=''",
            "success"   =>  "$('#datos_usuario_reserva').html(data); $('#usuario_existente').val(1);",
            'loading'   =>  '$("#validar-loader").show()',
            "complete"  =>  "$('#validar-loader').hide()",
            "method"    =>  "get",
            "with"      =>  "'codigo_usuario='+$('#codigo_usuario_reserva').val()"
            ))
?>
                </td>
            </tr>
    </table>
    <div id="datos_usuario_reserva"></div>
    <br><br>

            <div id="listado_ejemplares_reservados" align="center">
                <?php include_partial('opac/ejemplares_reservados',array('ejemplares'=>$ejemplares,'fichas'=>$fichas)) ?>
            </div>
            <?php echo input_hidden_tag('usuario_existente',0) ?>
            <?php echo input_hidden_tag('biblioteca',$biblioteca) ?>
    </form>
