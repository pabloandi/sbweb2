<?php use_helper("Form") ?>
<?php if(isset($ejemplares) && count($ejemplares)>0): ?>
    <table>
		<thead class="ui-dialog-titlebar ui-widget-header ui-corner-all">
			<tr>
				<th width="10%">Número de Adquisición</th>
				<th width="85%">Título</th>
				<th align="left" width="5%">Rechazar</th>
			</tr>
		</thead>
		<tbody>
    <?php foreach($ejemplares AS $key => $ejemplar): ?>
    <tr>
        <td><?php echo $ejemplar->getNumAdqui() ?></td>
        <td><?php echo $fichas[$key]->getTitulo() ?></td>
        <td align="left">
            <?php echo jq_link_to_remote(image_tag("admin/cancel.png","border='0' alt='rechazar'"),array(
                "url"       =>  "opac/rechazarEjemplar",
                "update"    =>  "listado_ejemplares_reservados",
                "loading"   =>  "$('#listado_ejemplares_reservados').html('loading')",
                "script"    =>  'true',
                "confirm"   =>  "Esta seguro de rechazar este ejemplar?",
                "method"    =>  "post",
    //             "with"      =>  "'ejemplar_rechazado=".$ejemplar->getFichaNo()."_".$ejemplar->getNumAdqui()."'"
                "with"      =>  "'ejemplar_rechazado=".$key."&biblioteca_actual_busqueda='+$(\"#biblioteca_actual_busqueda\").val()"
            )) ?>
            <?php echo input_hidden_tag('ejemplares_a_reservar[]',$ejemplar->getFichaNo()."_".$ejemplar->getNumAdqui()."_".$ejemplar->getBiblioteca()) ?>
        </td>
    </tr>
    <?php endforeach; ?>

    </tbody>
    </table>
    <table>
    <tr>
                    <td colspan="2" align="center">
                        <?php echo submit_tag('Solicitar reserva de ejemplares') ?>
                        <span id="reserva-ejemplares-loader" style="display:none;">
                            <?php echo image_tag('ajax-loader.gif','align=absmiddle') ?>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="respuesta_reserva"></div>
                    </td>
                </tr>
            
    </table>
<?php else: ?>
    No existen ejemplares reservados
<?php endif; ?>
