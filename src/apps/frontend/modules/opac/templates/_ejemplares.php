<?php use_helper("JQuery","Form") ?>

<div id="ejemplares">
<?php echo jq_form_remote_tag(
                array(
                    "url"       =>  "opac/agregarEjemplares",
                    "update"    =>  "empty",
                    "script"    =>  'true',
                    "complete"  =>  '
										$("#solicitar_reserva_loader").hide(); 
										//alert("su solicitud ha sido registrada");
										mensaje("su solicitud ha sido registrada");
										',
                    "condition" =>  'contarCheckboxesEnMostrarEjemplares()>0',
                    'loading'   =>  '$("#solicitar_reserva_loader").show()',
                    "method"=>"get"
                    ), array ("align" => "center"))
?>

<?php if($tipo=="R"): ?>
    <table width="550px" border="1" cellspacing="0" class="font-table">
        <tr class="ui-dialog-titlebar ui-widget-header ui-corner-all">
                    <th align="left"><div align="center">Numero adquisici&oacute;n</div></th>
                    <th align="left"><div align="center">Biblioteca</div></th>
                    <th align="left"><div align="center">A&ntilde;o</div></th>
                    <th align="left"><div align="center">Volumen</div></th>
                    <th align="left"><div align="center">N&uacute;mero</div></th>
                    <th align="left"><div align="center">Estado</div></th>
                    <th align="left">&nbsp;</th>
        </tr>
        <?php foreach ($ejemplares as $ejemplar): ?>
        <tr>
            <td>
                <?php echo $ejemplar->getNumAdqui() ?>            </td>
            <td>
                <?php echo $ejemplar->getBibliotecas() ?>            </td>
            <td>
                <?php echo $ejemplar->getAno() ?></td>
            <td>
                <?php echo $ejemplar->getVolumen() ?></td>
            <td>
                <?php echo $ejemplar->getNumero() ?>
            </td>
            <td>
                <?php echo $ejemplar->getEstado() ?>
                <?php //echo $ejemplar->getcheckbox() ?>
                <?php if($ejemplar->getEstado()=="Disponible" ): ?></td>
            <td><?php echo checkbox_tag('ejemplares_seleccionados[]',$ejemplar->getFichaNo()."_".$ejemplar->getNumAdqui()."_".$ejemplar->getBiblioteca(), false, array("class"=>"row_mostrar_ejemplares")) ?>              
            <?php endif; ?></td>
        </tr>
        <?php endforeach ?>
        
    </table>
<?php else: ?>

<table width="550px" border="1" cellspacing="0" class="font-table">
        <tr class="ui-dialog-titlebar ui-widget-header ui-corner-all">
                    <th align="left"><div align="center">Numero adquisici&oacute;n</div></th>
                    <th align="left"><div align="center">Biblioteca</div></th>
                    <th align="left"><div align="center">Ejemplar</div></th>
                    <th align="left"><div align="center">Volumen</div></th>
                    <th align="left"><div align="center">Estado</div></th>
                    <th align="left">&nbsp;</th>
        </tr>
        <?php foreach ($ejemplares as $ejemplar): ?>
        <tr>
            <td>
                <?php echo $ejemplar->getNumAdqui() ?>            </td>
            <td>
                <?php echo $ejemplar->getBibliotecas() ?>            </td>
            <td>
                <?php echo $ejemplar->getEjemplar() ?></td>
            <td>
                <?php echo $ejemplar->getVolumen() ?></td>
            <td>
                <?php echo $ejemplar->getEstado() ?>
                <?php //echo $ejemplar->getcheckbox() ?>
                <?php if($ejemplar->getEstado()=="Disponible"  ): ?></td>
            <td><?php echo checkbox_tag('ejemplares_seleccionados[]',$ejemplar->getFichaNo()."_".$ejemplar->getNumAdqui()."_".$ejemplar->getBiblioteca(), false, array("class"=>"row_mostrar_ejemplares")) ?>              
            <?php endif; ?></td>
        </tr>
        <?php endforeach ?>
        
    </table>

<?php endif; ?>

            <div align="center">
                <br /><?php echo submit_tag("Agregar a la lista de solicitudes") ?>
                <span id="solicitar_reserva_loader" style="display:none;">
                    <?php echo image_tag('ajax-loader.gif') ?>                </span>          <br />  
            </div>
        
		
		
    </form>
</div>
