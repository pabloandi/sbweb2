<?php use_helper('Form') ?>
		<table >
			<tr>
					<td>Nombre del usuario</td>
					<td><?php echo input_tag('nombre_usuario_reserva',$usuario->getNombre(),'readonly=readonly size=32') ?></td>
				</tr>
				<tr>
					<td>Correo del usuario</td>
					<td><?php echo input_tag('correo_usuario_reserva',$usuario->getEmail(),'size=32') ?></td>
				</tr>
				<tr>
					<td colspan=2>Nota: Si el correo que se muestra aquí, está erróneo o no aparece, por favor digitarlo en el campo de "Correo de usuario".</td>
				</tr>
		</table>
