<?php use_helper("Form",'PagerNavigation') ?>


<form id="form_tabla_consulta">
<table align="center" width="90%" id="tabla_consulta" class="tabla-consulta">

<thead class="ui-dialog-titlebar ui-widget-header ui-corner-all" align="center">
    
    <th>Título</th>
    <th>Autor</th>
    <th>Clasificación</th>
    <th>Tipo de material</th>
    <th>Biblioteca</th>
</thead>
    <tbody>
    <!-- fichas -->
    <?php foreach($pager->getResults() AS $ficha): ?>
        <tr id="<?php echo $ficha->getFichaNo()."-".$ficha->getBiblioteca() ?>" class="fila_ficha" style="cursor: pointer;">
            <td><?php echo $ficha->getTitulo() ?></td>
            <td><?php echo $ficha->getAutor() ?></td>
            <td><?php echo $ficha->getClasificacion() ?></td>
            <td><?php echo $ficha->getTipoMaterial() ?></td>
            <td><?php echo $ficha->getBibliotecas() ?></td>
        </tr>
    
    <?php endforeach; ?>
    <!-- termina fichas -->
    
    </tbody>
</table>
<br>
<div >
	<?php echo pager_navigation($pager,array('url'=>"opac/consultar?txtLIB=$txtLIB&categoria=$categoria",'update'=>'resultado_consulta','loading'=>'$("#mini-loader").show()','complete'=>'$("#mini-loader").hide(); cargarTabla();')) ?>
	&nbsp;<span id="mini-loader" style="display:none;">    	<?php echo image_tag('ajax-loader.gif') ?> </span>
	 &nbsp; <?php echo $pager->getNbResults(); ?> Coincidencias

</div>
</form>

<script>
cargarSeleccionFichas();
</script>
