<?php
$_SERVER["REQUEST_URI"] = str_replace($_SERVER["SCRIPT_NAME"], "", $_SERVER["REQUEST_URI"]);
//echo $_SERVER["REQUEST_URI"];

# FROZEN_SF_LIB_DIR: /var/www/production/sfweb/www/cache/symfony-for-release/1.2.4/lib

require_once dirname(__FILE__).'/../lib/symfony/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
    // for compatibility / remove and enable only the plugins you want
    $this->enableAllPluginsExcept(array('sfDoctrinePlugin', 'sfCompat10Plugin'));
    
    sfConfig::set('php_mailer',sfConfig::get('sf_lib_dir').'/phpmailer/class.phpmailer.php');
    
    $this->setWebDir($this->getRootDir().'../../html');
    
  }
}
