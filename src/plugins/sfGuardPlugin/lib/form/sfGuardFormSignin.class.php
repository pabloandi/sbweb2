<?php

class sfGuardFormSignin extends sfForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'username' => new sfWidgetFormInput(),
      'password' => new sfWidgetFormInput(array('type' => 'password')),
      'remember' => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'username' => new sfValidatorString(array(),array(
		'required'	=>	'Por favor debe digitar el nombre de usuario',
		'invalid'	=>	'Nombre de usuario y/o contraseña inválida',
      )),
      'password' => new sfValidatorString(array(),array(
		'required'	=>	'Por favor debe digitar la contraseña',
		
      )),
      'remember' => new sfValidatorBoolean(),
    ));

    $this->validatorSchema->setPostValidator(new sfGuardValidatorUser());

    $this->widgetSchema->setNameFormat('signin[%s]');
  }
}
