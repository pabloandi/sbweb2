
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- EjemplaresGeneral
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `EjemplaresGeneral`;


CREATE TABLE `EjemplaresGeneral`
(
	`Ficha_No` VARCHAR(11)  NOT NULL,
	`NumAdqui` VARCHAR(15)  NOT NULL,
	`Biblioteca` INTEGER(11)  NOT NULL,
	`Ano` INTEGER(11),
	`Volumen` INTEGER(11),
	`Numero` INTEGER(11),
	`Ejemplar` INTEGER(11),
	PRIMARY KEY (`Ficha_No`,`NumAdqui`,`Biblioteca`),
	INDEX `EjemplaresGeneral_FI_1` (`Biblioteca`),
	CONSTRAINT `EjemplaresGeneral_FK_1`
		FOREIGN KEY (`Biblioteca`)
		REFERENCES `Bibliotecas` (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- EtiquetasMarc
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `EtiquetasMarc`;


CREATE TABLE `EtiquetasMarc`
(
	`idregistro` INTEGER(11)  NOT NULL,
	`idficha` INTEGER(11),
	`numetiqueta` INTEGER(11),
	`contenido` TEXT,
	`posicion` INTEGER(11),
	`biblioteca` INTEGER(11)  NOT NULL,
	PRIMARY KEY (`idregistro`,`biblioteca`),
	INDEX `EtiquetasMarc_FI_1` (`biblioteca`),
	CONSTRAINT `EtiquetasMarc_FK_1`
		FOREIGN KEY (`biblioteca`)
		REFERENCES `Bibliotecas` (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- FichasGeneral
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `FichasGeneral`;


CREATE TABLE `FichasGeneral`
(
	`Ficha_No` VARCHAR(11)  NOT NULL,
	`EtiquetasMARC` TEXT,
	`Titulo` TEXT,
	`Autor` TEXT,
	`Clasificacion` TEXT,
	`Isbn` VARCHAR(50),
	`FechaPublicacion` VARCHAR(20),
	`TipoMaterial` VARCHAR(50),
	`Biblioteca` INTEGER(11)  NOT NULL,
	PRIMARY KEY (`Ficha_No`,`Biblioteca`),
	INDEX `FichasGeneral_FI_1` (`Biblioteca`),
	CONSTRAINT `FichasGeneral_FK_1`
		FOREIGN KEY (`Biblioteca`)
		REFERENCES `Bibliotecas` (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- MultasPendientes
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `MultasPendientes`;


CREATE TABLE `MultasPendientes`
(
	`IdMulta` INTEGER(11)  NOT NULL,
	`NoCuenta` VARCHAR(20),
	`Nombre` VARCHAR(60),
	`Escuela` VARCHAR(100),
	`FechaDevolucion` DATETIME,
	`Monto` INTEGER(19),
	`Observaciones` TEXT,
	`IdCapturista` VARCHAR(10),
	`IdBloqueo` INTEGER(11),
	`Biblioteca` INTEGER(11)  NOT NULL,
	PRIMARY KEY (`IdMulta`,`Biblioteca`),
	INDEX `MultasPendientes_FI_1` (`Biblioteca`),
	CONSTRAINT `MultasPendientes_FK_1`
		FOREIGN KEY (`Biblioteca`)
		REFERENCES `Bibliotecas` (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- PrestamosGeneral
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `PrestamosGeneral`;


CREATE TABLE `PrestamosGeneral`
(
	`NoCuenta` VARCHAR(20)  NOT NULL,
	`NumAdqui` VARCHAR(15)  NOT NULL,
	`Nombre` VARCHAR(50),
	`Titulo` VARCHAR(60),
	`Autor` VARCHAR(50),
	`Clasificacion` VARCHAR(50),
	`FechaSalida` VARCHAR(16),
	`FechaEntrega` VARCHAR(16),
	`TipoPrestamo` VARCHAR(16) default '' NOT NULL,
	`Biblioteca` INTEGER(11)  NOT NULL,
	PRIMARY KEY (`NoCuenta`,`NumAdqui`),
	INDEX `PrestamosGeneral_FI_1` (`Biblioteca`),
	CONSTRAINT `PrestamosGeneral_FK_1`
		FOREIGN KEY (`Biblioteca`)
		REFERENCES `Bibliotecas` (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- Reservados
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `Reservados`;


CREATE TABLE `Reservados`
(
	`numadqui` VARCHAR(15)  NOT NULL,
	`fechainicial` DATETIME,
	`fechafinal` DATETIME,
	`NoCuenta` VARCHAR(20),
	`Biblioteca` INTEGER(11)  NOT NULL,
	PRIMARY KEY (`numadqui`),
	INDEX `Reservados_FI_1` (`Biblioteca`),
	CONSTRAINT `Reservados_FK_1`
		FOREIGN KEY (`Biblioteca`)
		REFERENCES `Bibliotecas` (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- UsuariosGeneral
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `UsuariosGeneral`;


CREATE TABLE `UsuariosGeneral`
(
	`NoCuenta` VARCHAR(20)  NOT NULL,
	`Nombre` VARCHAR(50)  NOT NULL,
	`NoGrupo` VARCHAR(50)  NOT NULL,
	`NoEscuela` VARCHAR(250)  NOT NULL,
	`Email` VARCHAR(50),
	`Domicilio` VARCHAR(50),
	`Colonia` VARCHAR(50),
	`CiudadEstado` VARCHAR(50),
	`Telefono` VARCHAR(50),
	`Notas` VARCHAR(120),
	`Multa` VARCHAR(50),
	`Vigencia` VARCHAR(50), --adicion vigencia
	PRIMARY KEY (`NoCuenta`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- Bibliotecas
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `Bibliotecas`;


CREATE TABLE `Bibliotecas`
(
	`idbiblioteca` INTEGER(11)  NOT NULL,
	`nombre` VARCHAR(250)  NOT NULL,
	`nombrecorto` VARCHAR(50),
	`director` VARCHAR(60),
	`telefono` VARCHAR(100),
	`correo` VARCHAR(100),
	`siabucversion` INTEGER(1) default 9,
	`activo` INTEGER(1),
	PRIMARY KEY (`idbiblioteca`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- Buzon
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `Buzon`;


CREATE TABLE `Buzon`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50)  NOT NULL,
	`correo` VARCHAR(50)  NOT NULL,
	`asunto` VARCHAR(50),
	`mensaje` TEXT,
	`biblioteca` VARCHAR(60),
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`id`)
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- SolicitudesPrestamo
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `SolicitudesPrestamo`;


CREATE TABLE `SolicitudesPrestamo`
(
	`id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nocuenta` VARCHAR(15)  NOT NULL,
	`numadqui` VARCHAR(15)  NOT NULL,
	`usuario` VARCHAR(50),
	`titulo` VARCHAR(60),
	`autor` VARCHAR(50),
	`clasificacion` VARCHAR(50),
	`estado_id` INTEGER default 1,
	`created_at` DATETIME,
	`updated_at` DATETIME,
	PRIMARY KEY (`id`),
	INDEX `SolicitudesPrestamo_FI_1` (`estado_id`),
	CONSTRAINT `SolicitudesPrestamo_FK_1`
		FOREIGN KEY (`estado_id`)
		REFERENCES `Estado` (`id`)
		ON DELETE NO ACTION
)ENGINE=MyISAM;

#-----------------------------------------------------------------------------
#-- Estado
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `Estado`;


CREATE TABLE `Estado`
(
	`id` INTEGER  NOT NULL,
	`nombre` VARCHAR(50),
	PRIMARY KEY (`id`)
)ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
